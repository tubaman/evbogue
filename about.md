---
title: About 
---

Hi, I'm Ev Bogue. My full name is Everett Bogue.

Things you should know about me...

1. I do not use centralized social media
1. I don't use anything Google either
1. And I think you are an idiot if you do
1. I run an Arch Linux box on a < $400 machine I bought at Walmart last year
1. In my past lives I've worked at Apple, Nymag, and NYU
1. Since 2009 I've lived in Brooklyn, Portland, Oakland, San Francisco, Puerto Vallarta, Singapore, Tokyo, Berlin, KCMO, and Seattle. Not in that order, and there were some repeats. Now I live in la ciudad de Mexico.
1. I make all of my income from technical products
1. This is why I can live anywhere in the world
1. I wrote [Develop Your Business](http://dev.evbogue.com) because I get endless questions about how I run my business
1. I hate bullshit and bullshitters
1. I use Xmonad as my window manager
1. I use Vim to write this blog
1. I don't live in los Estados Unidos because I think it's a shitshow up there
1. I think that most Mexicans are better off than Americans
1. I wrote [Design Your Website](http://design.evbogue.com) because there are no good resources to learn HTML/CSS on the Internet
1. I was most famous for fucking minimalism in 2011
1. And yet all of my shit still fits in one bag
1. I hate it when people talk out of both sides of their necks. IE: "I hate Facebook" and then they log in every fifteen minutes. This is one of the most disgusting things a human can do. If you disagree with something, don't do it at the same time. Duh.
1. I think that Mark Zuckerberg has done more harm to the human race than any other person alive right now
1. But I also blame people who continue to suck on his tit. At some point you gotta realize the milk formula is bad
1. I think the US government is trying to kick off World War III with Russia. But Russia isn't taking the bait
1. Because if they don't, the United States economy will collapse and the politicans will be blamed. And they should be
1. They also want to take away everyone's guns in America. But if they do they will start a very bloody civil war. They would also love to do that as well
1. I don't own a cell phone. I sold mine in 2012 to a resale shop in San Francisco
1. The only time I've been asked for my phone number since was by Pizza Hut, who wouldn't deliver a pizza without knowing my number. So I had to pick the pizza up
1. Why? Because I don't want all of my coversations recorded by a device that's been proven to record your conversations and ship them off to the Utah data center
1. I'll have a mobile device again when they are open source and mesh network enabled
1. I drink
1. I wrote [Build Your Perfect Machine](http://build.evbogue.com), because I think the single most important thing you can do right now is take control of your computer
1. Because if you don't, then your computer can be used to spy on you or worse
1. Also using Linux makes everyone think you're very bad-ass

