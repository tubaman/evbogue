---
title: Why I switched back to Arch
date: 2014-04-09
---

A few readers got in touch in various ways after my Debian post to ask the obvious question...

"Why the hell would you switch from Arch to something half as good?"

This is an excellent question. 

To answer the question, I have switched back to Arch Linux. I could only hack having Debian on my local machine for 3 days and on my VPS for 7. 

The reason? Debian isn't as awesome as Arch.

I try other Linux distributions because I want to make sure I'm using the best thing. The best is my quest. I want to [build my perfect machine](http://build.evbogue.com) and this means sometimes experimenting with other operating systems and distributions to make sure I'm using the best one.

The bad news for Debian is it isn't the best. 

The primary reason I switched back was for the Arch Linux community repository, [AUR](http://aur.archlinux.org). Without the community repository I was stuck building cutting edge applications myself. Once they were built, I had to figure out how to remove them if I needed to. With Arch Linux I can use Cower to install cutting edge applications (such as Bitmessage, the Tor Browser, and Electrum) and then I can use Pacman to remove them if needed. 

I realize a few people were very enthusiastic to hear I'd switched to Debian. They felt this was an endorsement. I'm sure these people will be disappointed to hear I've switched back to Arch. But as the many people who use Arch who also read this site mentioned... "why would you switch away from the best?"

I couldn't stay away for long.
