---
title: S. America
date: 2014-06-08
---

I developed my business in a location independent way so I could travel anywhere in the world. One of my big dreams four years ago when I was living in San Francisco was to throw all of my shit into a bag and head down to South America. There I imagined myself wandering around from city to city figuring out where was cool, and where is not down there.

Now I have a different reason. I want to get as far away from North America as possible. I'm stressed. I can't sleep at night. Too many people are thumbing themselves in the ears and pretending that everything is just fine. But anyone who's following actual reported news in the world (not CIA backed CNN or Fox News) knows that things are not fine. 

So I have this double edged plan of both living the life I wanted to live four years ago, and also finding a way to get farther away from the collapse of Rome.

I realize it's no longer popular to tell the truth or say what is actually up, but I'm going to continue to do it anyway. 

The only way I know to keep doing the work, as well as traveling the world, is to continue to learn and use a free and open Internet. You won't find my posts on social media. Not because they're being filtered out of the stream, but because I'm not engaging in that stream to begin with.

If you want to hear from me, subscribe to my Bitmessage address or check back to this website every day.

