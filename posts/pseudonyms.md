---
title: Pseudononymous Digital Selves 
date: 2013-12-25
---

Use a pseudonym to learn, use your real name to earn. 

For the last quarter of 2012, and the first half of 2013, I experimented with using a pseudonym. His name was Ven Portman.

I'd always dreamed of having a pseudonym to maintain my digital presence, while I lived and worked from anywhere. So I figured, why not create another avatar and try to make a living with him?

The trouble was, I couldn't get anyone to buy products from my new pseudonym. By not using my real name, I lost a significant amount of trust. Who is this Ven guy anyway?

Ev Bogue was far more profitable than Ven Portman this year. The reason: it's my real name.

At one point I merged Ven Portman and Ev Bogue and tried to switch names. It all ended up being very confusing for the readers of this site. It was also unexpectedly confusing to my own brain. Am I Ven, or Ev? I was confused too.

There were benefits to using a pseudonym. I'd recommend it to anyone, but only for one reason: learning.

When I started learning to code, I was intimidated by showing my commits in public. Because I have a significant group of people who follow my work, I didn't want them to see just how much of a newb I was for a couple of months.

Using Ven allowed me to learn in a rapid fire way, without anyone knowing it was Ev Bogue who was doing the learning. 

However, for selling anything online, I can't recommend using a pseudonym. People need to trust you, and me, in order to buy from you.



