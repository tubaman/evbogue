---
title: Virtual Debian with systemd
date: 2014-04-01
---

With systemd, all of your programs have the option of checking in and telling the system whether or not they're running.

I can't imagine life without systemd. How else would I make sure everything on my system is working by typing

	systemctl

Linux without systemd isn't worth using. Which is why I was thrilled to hear that Debian adopted systemd. Arch Linux has shipped with systemd since 2012, so that's been my default operating system. Now that Debian supports systemd, I thought I'd give it a test drive. Here are the benefits to doing so.

+ Debian has a large userbase of hardcore peeps
+ Debian is super more stabler than Arch Linux
+ Debian has been around forever and is downstream to most Linux distros
+ Debian is used by all homeless hackers on the planet for good
+ Saying you can install Debian will get you laid or grow you a neckbeard

But what the hell is systemd? Well, let me crack open a Modelo Especial and tell you. systemd is the first PID that fires up when your Linux system launches. systemd is based on launchd, which is the init manager for Apple's OS X. You know why your Mac started up so fast (before you threw it in the trash)? launchd. You know why Arch Linux and now Debian starts up so fast? systemd. systemd has brought a lot of stability to the Linux platforms that have adopted it. Without systemd, Linux is all crashy, because all of the programs are written by different developers. Some of these developers are shit, and others are brilliant. systemd can keep you from having a lot of heartache when you wake up in the middle of the night to discover that your web server crashed because you were on XXX and now you have to restart it. Why? systemd will restart your web server for you when it notices that it crashes. No other daemon is good at this, I know because I've tried them all.

The reason I became interested in systemd was my web servers kept crashing, and I didn't know why. I didn't care why. I just wanted them to stay up. Now, I sleep at night knowing systemd is always watching out for me like a guardian fucking angel sitting at PID 1 and waiting for something to go wrong. 

Now that Debian uses systemd it will be relevent again. Ditto for Ubuntu, if they can ever figure out how to implement it.

Are you ready to dive in? Alright, lets dive in.

This is how I installed Debian on my VPS and upgraded it to Jessie with systemd.

Let's do this!

First, navigate your ass over to [Digital Ocean and get a $5 VPS with this referral link](https://www.digitalocean.com/?refcode=26d8ed49730d). You'll help me pay my VPS bills. Great.

When you log into Digital Ocean select 'Droplets', then click 'Create'. 

Type in a hostname. How about "newyork"?

Select 512MB
Select New York 1 or 2
Click Debian and pick Debian 7.0 x64 

Leave the rest of the settings as default, and click Create Droplet.

You will get an email with a username "root" and password. To log in, use your Terminal (you do have a terminal, don't you?) and ssh in.

	$ ssh root@your.ip.address

Type your password when prompted.

You're in!

Next we'll be upgrading Debian to the unreleased version, Debian Jessie, and then switching systemd on for maximum awesomeness. 

First, we have to tell Debian that we want it to be Jessie, and not <del>Yeezus</del> Wheezy. We don't want Wheezy, because it doesn't support systemd. We want Jessie, because Jessie has systemd, and systemd is awesome.

To upgrade to Jessie we have to edit a file.

First install vim. You do know how to use vim don't you?

	$ apt-get update
	$ apt-get install vim

And then edit the file

	$ vim /etc/apt/sources.list

This is how the file will look

	deb http://ftp.us.debian.org/debian wheezy main
	deb http://security.debian.org/ wheezy/updates main

We want to change this to jessie. Can you guess how to do this? Replace wheezy with jessie.

W00t, you did it. Now save your file using 

	:wq

Now, upgrade Debian to jessie, because right now it's still wheezy. To do this type 

	$ apt-get update
	$ apt-get dist-upgrade

Now it prompts you with a very long list of packages that it will install. Type 'y' and watch the magic happen. If your girlfriend is in the room while this happens, she will be very impressed. Drink a beer, this might take awhile.

When it's done, you have Jessie. Congrats, you're on the cutting edge of Debianness.

It'll probably dump you into a security log. Just scroll past that, or read the whole thing if you have all day.

Then select your keyboard layout, and language. 

Select 'yes' to the 'restart services' prompt. 

If it asks you to upgrade sysctl.conf, type 'Y' for yes.

After the next install messages, you should be DONE with upgrading to jessie.

jessie is the next version of Debian, so it might not work as planned sometimes. Though, I haven't had any trouble. This is just a warning, to let you know, that you're on the edge. 

Alright, next we gotta install systemd. Because Debian without systemd would just be shit. Why? Because you wouldn't have a control center to turn your services on and off. Also your shit might crash, and stay down in the middle of the night. Systemd keeps your shit up. So you can sleep at night.

To install systemd

	$ apt-get update
	$ apt-get install systemd

Next, to make sure systemd runs when you reboot install

	$ apt-get install systemd-sysv

Yay! Now reboot.

Log in again. You're going to need that password again, so keep the email from [Digital Ocean](https://www.digitalocean.com/?refcode=26d8ed49730d) (this rewards link is for those of you who didn't click the one above).

And type

	$ systemctl

To see if systemd works.

If it works, you should see...

	UNIT                          LOAD   ACTIVE SUB       DESCRIPTION
	proc-sys-...mt_misc.automount loaded active waiting   Arbitrary Executable File Form
	sys-devic...-block-sr0.device loaded active plugged   QEMU_DVD-ROM
	sys-devic...0-net-eth0.device loaded active plugged   Virtio network device
	sys-devic...-block-vda.device loaded active plugged   /sys/devices/pci0000:00/0000:0
	sys-devic...-tty-ttyS2.device loaded active plugged   /sys/devices/platform/serial82
	sys-devic...-tty-ttyS3.device loaded active plugged   /sys/devices/platform/serial82
	sys-devic...-tty-ttyS0.device loaded active plugged   /sys/devices/pnp0/00:06/tty/tt
	sys-devic...-tty-ttyS1.device loaded active plugged   /sys/devices/pnp0/00:07/tty/tt
	sys-module-fuse.device        loaded active plugged   /sys/module/fuse
	sys-subsy...vices-eth0.device loaded active plugged   Virtio network device

If you see that, you have systemd and are now awesome. Congrats for being awesome. 

Next step, buy [Build Your Perfect Machine](http://build.evbogue.com/) and learn to build your perfect machine.

-Ev
