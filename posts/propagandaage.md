---
title: Writing in the propaganda age 
date: 2014-06-12 
---

I deleted the post I wrote yesterday for today.

Instead I want to write about being a writer in the propaganda age.

We're living in the propaganda age. We thought it was the information age, but instead we got a giant tool for the government to use as propaganda. And the real scary thing is most people cram it down their own throats.

When I was a teenager, I was in a band called Propaganda. We played Green Day covers. The lead guitarist always turned his amp up louder than mine because he didn't want know that I played better than him. Right now the noise level is turned up to deafening by a team of 20-somethings without souls in D.C. under John Kerry's orders.

But I want to write about current affairs right now, because I don't think anyone is going to hear about them. Everyone I talk to is blissful in their unawareness of world affairs. To be specific, where los Estados Unidos comes into play. Remember ten years ago when everything single bomb in Iraq was reported by a horde of media? Not the media only reports giant world events or what comes from the White House press office.

So you might not know that The State Department of The United States backed a violent mob in the streets of Kiev that overthrew the democratic government of the Ukraine earlier this year. You might not know that they gave this government money to hire a sniper, and then have that sniper shoot at both sides of the conflict. Protesters and police. You might not know that the United States has promised the Ukraine 1 Billion USD, but only if they use their military against civilians who don't believe the US installed puppet government in Kiev is a sham.

You also might know that the United States government is sending large amounts of lethal weapons to "moderate" rebel groups in Syria. You also might not know that rebel groups from Syria are now taking control of most of the cities "liberated" by the United States government in Iraq. 

Where are all of these countries anyway? Somewhere near Japan? If you can't get a map out you're a dumbass.

You have access to maps and you can see that Syria borders Iraq to the West and because the United States has been attempting to destablize the democratically elected government of Syria for three years that the Syria border allows all of those United States weapons to leak over the border into Iraq. 

You might also not know that two days ago a Blackhawk Helicopher that The United States left parked in Iraq without any guards has been 'liberated' from the United States installed puppet government of Iraq and is now in the hands of the rebels that the United States has been armying in Syria.

You might not know about all of this because almost none of it has been reported in the mainstream media. Instead you hear about everything the government wants you to hear about. These are distractions from the big evil shit that's going on. 

So when you ask me "why would you ever want to get farther away from The United States?" I say wake up. It's not 1995 anymore. 

I've resigned myself to the fact that I'll be a writer from now until I am no longer.

And part of writing is attempting to cut through the bullshit in order to tell the truth. This is why I wrote yesterday about how I fucked up and wasn't able to get on a plane to Colombia, because if I'd pretended 'everything was okay!' I'd be lying to you.

Now writing is a craft. The attempt is to craft the truth. So nothing will get at the whole true. But deliberate lying every single day, as some bloggers do, is not the way to get at the truth. Even if you're scared, bullshitting won't help take away that terrified feeling deep within your soul. 

A writer has to resign himself to attempting to tell the truth of the moment, no matter how hard it is for people to hear.

That's why my [most useful product](http://build.evbogue.com) is also my least popular one. Because it's the hardest one, but also the most necessary to the advancement of your humanity.

But you're not going to buy it, are you? You're going to keep sucking pipe and listening to what they tell you, aren't you?### Writing in the propaganda age 

**Jun 12 2014**

I deleted the post I wrote yesterday for today.

Instead I want to write about being a writer in the propaganda age.

We're living in the propaganda age. We thought it was the information age, but instead we got a giant tool for the government to use as propaganda. And the real scary thing is most people cram it down their own throats.

When I was a teenager, I was in a band called Propaganda. We played Green Day covers. The lead guitarist always turned his amp up louder than mine because he didn't want know that I played better than him. Right now the noise level is turned up to deafening by a team of 20-somethings without souls in D.C. under John Kerry's orders.

But I want to write about current affairs right now, because I don't think anyone is going to hear about them. Everyone I talk to is blissful in their unawareness of world affairs. To be specific, where los Estados Unidos comes into play. Remember ten years ago when everything single bomb in Iraq was reported by a horde of media? Not the media only reports giant world events or what comes from the White House press office.

So you might not know that The State Department of The United States backed a violent mob in the streets of Kiev that overthrew the democratic government of the Ukraine earlier this year. You might not know that they gave this government money to hire a sniper, and then have that sniper shoot at both sides of the conflict. Protesters and police. You might not know that the United States has promised the Ukraine 1 Billion USD, but only if they use their military against civilians who don't believe the US installed puppet government in Kiev is a sham.

You also might know that the United States government is sending large amounts of lethal weapons to "moderate" rebel groups in Syria. You also might not know that rebel groups from Syria are now taking control of most of the cities "liberated" by the United States government in Iraq. 

Where are all of these countries anyway? Somewhere near Japan? If you can't get a map out you're a dumbass.

You have access to maps and you can see that Syria borders Iraq to the West and because the United States has been attempting to destablize the democratically elected government of Syria for three years that the Syria border allows all of those United States weapons to leak over the border into Iraq. 

You might also not know that two days ago a Blackhawk Helicopher that The United States left parked in Iraq without any guards has been 'liberated' from the United States installed puppet government of Iraq and is now in the hands of the rebels that the United States has been armying in Syria.

You might not know about all of this because almost none of it has been reported in the mainstream media. Instead you hear about everything the government wants you to hear about. These are distractions from the big evil shit that's going on. 

So when you ask me "why would you ever want to get farther away from The United States?" I say wake up. It's not 1995 anymore. 

I've resigned myself to the fact that I'll be a writer from now until I am no longer.

And part of writing is attempting to cut through the bullshit in order to tell the truth. This is why I wrote yesterday about how I fucked up and wasn't able to get on a plane to Colombia, because if I'd pretended 'everything was okay!' I'd be lying to you.

Now writing is a craft. The attempt is to craft the truth. So nothing will get at the whole true. But deliberate lying every single day, as some bloggers do, is not the way to get at the truth. Even if you're scared, bullshitting won't help take away that terrified feeling deep within your soul. 

A writer has to resign himself to attempting to tell the truth of the moment, no matter how hard it is for people to hear.

That's why my [most useful product](http://build.evbogue.com) is also my least popular one. Because it's the hardest one, but also the most necessary to the advancement of your humanity.

But you're not going to buy it, are you? You're going to keep sucking pipe and listening to what they tell you, aren't you?
