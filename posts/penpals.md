---
title: Bitmessage Pen Pals
date: 2014-03-29
---

A lot of you out there are using Bitmessage. I know this because I have around 40 people in my Bitmessage address book from the past few weeks.

The trouble is, many of you have no one to Bitmessage with. Bitmessage is not the most amazing tool for discovering new friends. It is, however, very good for having intimate conversations.

To this end, I want to create a Bitmessage Pen Pals page. This way you can Bitmessage random people and see what they're up to.

Below I'll add people who are using Bitmessage and want to be Bitmessaged. To be added to the list, Bitmessage me with the following information

1. Your full name (no one wants to Bitmessage "E")
2. Your website (if you have one)
3. Your Bitmessage address

This is how your entry will look

	Ev Bogue	http://evbogue.com	BM-2cV8cTUxCx5uFFyNy2E3mo66Xp3nrZFFG2

Bitmessage me if you want to be added to the Bitmessage pen pals list!

### Bitmessage Pen Pals

	Ev Bogue	http://evbogue.com		BM-2cV8cTUxCx5uFFyNy2E3mo66Xp3nrZFFG2
	Meira Datiya					BM-2cWeVuoQj7kPtWZPwXMhin9r78KeAWLNw8
	Andrew Shell	http://blog.andrewshell.org/	BM-2cTBKC31biugJg1dt1VCsFnzZfhGs7BQHR
	Lin Winterstrang				BM-2cVSuxqj4TGTAzzdtS8D3WBb4N4j6XQ4fP
	Gwen Bell	http://gwenbell.com		BM-2cUBURK3VQaVXEJiUMW8kZ22GrRZNA8kQ6
	Samy Dindane 	http://samy.dindane.com 	BM-NC5vvFN6r5PBZFLgXxk83wBKmypKTVR1
	Kelly Montgomery 				BM-2cTYSuQBDoy9RkpEV4gAnS7e6extC7kiLy
	Herbert Waldemere http://herbertwaldemere.com/	BM-2cUNvW4KtDVXHP6ZWyrR68ZR5QvZRnFkmW
	Miles Moyers	http://mil3s.com 	   	BM-2cVLkpWjBsVai43VuLs1CdYYjKSC61uoFJ
	justin orvis steimer http://justinsteimer.com	BM-2cVRJ8cAv64PjNiY4Y6VNP6aX52qK3MjAq
	Radhika Morabia	http://rmorabia.com		BM-2cVAA36JjjVtd7QWz4hatxqbfoQkL1Cq7J (new address)	
	Billy    	http://www.genebio.net     	BM-2cTVXhtiFZ5YEvGGdLXQa2PiLPZueu4xhV
	Maggie Dentakos					BM-NBCkAz5SkL5bh97oWwxmSQCB4mpFCM5Q (new address)
	Tyron Love 	http://www.tyronlove.com	BM-2cWQNb1sAj1JoLCHcZqGojaSYLHeAQNwND
	Tomasz Jadowski http://tjadowski.sdf.org   	BM-GuN3buVKd9tD8x3tDBbTEks1Fpv1J62w
	Marco Larrazabal http://malarrz.com		BM-2cWCBQcTPS3utvTbzZGT6EWyL2wrEcEw56
	Dan O'Prey	http://oprey.co.uk		BM-2cXktA45vXdKxvvKdJRgLrq5vL7YryjhwE
	Joseph Post	http://jcpst.com		BM-2cWJQNhJH1HzwBNognVcPQZBa6bt6fQCZT
	Robert Witham	http://robertwitham.com		BM-2cXYDXurRd5TpdygoxfV3tqFNq6Vwamdyw
	Seth		https://sysfu.com		BM-NBuDAqdf4zrH1GZzYzZt2UbjePnpJVka
	Sandra Begotka	http://begotka.com		BM-2cXEif9STmZEJtQYer8jxPyZYkfafqHgEe
	James Gager	http://jamesgager.com/		BM-2cSroJoxhas8K1NLeqsYrxfQQAY3cA3maU
	Ole Rosendahl  	http://hyaena.de     		BM-2cVLkitXhsBhdGijSoGMirFAiXv7Env4PE
	Klaus Alexander Seistrup http://klaus.seistrup.dk/ BM-BbvXWtfhr5nENu4TQtnxif16SUCdNKAF
	Tedd Martin					BM-2cTHZJYbUBnXa6vFNCaVVbYJ9rZuajd1CD

After you read this list of names, pick one to five people and send them a message. Introduce yourself and ask them some questions about themselves. Don't wait to get messed, reach out to some people!



