---
title: New (top secret) Project
date: 2014-06-28
---

I'm working on a new project. It's top secret, for now.

I'll give you a hint, it's about Internet privacy.

Here's the deal. If you send me a PGP/GPG encrypted message on or before July 4th 2014, I'll send you a link to my new project for free. It won't be finished yet, but I'm hardcore into the writing process.

Here's my [public key](http://evbogue.com/static/pubkey.asc)

When you email, make sure to attach a copy or the location of your pubkey so I can send an encrypted reply to you.


