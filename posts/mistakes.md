---
title: Mistakes 
date: 2013-11-10
---

This is my mistakes page. Inspired by [Gwern's mistakes page](http://www.gwern.net/Mistakes).

On this page, I'm going to write about a series of unfortunate mistakes I've made throughout my life, from Ev Bogue begging for money to believing in The Singularity. As I think of mistakes, I'll post them here. Over time I'll build a comprehensive list of mistakes I've made.

#### Deleting my website 

This is the mistake that lead to "Ev Bogue begging for money." I moved to Berlin in late May-2012, and by July I ran out of cash. I credit over-reliance on social media with my business downfall. I've made money online from digital products since 2009, and the only time I've had significant problems making an income was when I did this.

Without a website, I had no way of establishing authority in the digital space. When you're smushed into the sparse communist-block-esque rooms of a social network it's hard to tell talent from mediocrity. When I created a website, I started earning again. But it took half a year to build up the momentum I needed to start living anywhere again.

When I ran out of money, I asked on a social network. The trouble was, I didn't ask for anything in return. In fact, selling my Macbook Air led to far more cashflow than begging did. I can't recommend begging online to anyone, it just doesn't work unless you have cancer or starving children in a third-world country. And even then, you might be better off with a product or service.
