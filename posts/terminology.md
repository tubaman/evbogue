---
title: Termin-fuckin-ology
date: 2014-01-27
---

The terminal emulator I use on Arch Linux is [Terminology](http://enlightenment.org/p.php?p=about/terminology&l=en).

Terminology is built on [EFL](http://enlightenment.org/), a set of libraries produced by a guy named [Carsten Haitzler](https://en.wikipedia.org/wiki/Carsten_Haitzler) who goes by the handle of Raster. Raster works for Samsung in S. Korea on a project called [Tizen](https://www.tizen.org/). Tizen is a Linux window manager for tablets and mobile devices. You probably won't see it much until Android falls out of favor -- which could happen quite fast, you never know. Raster splits time between Tizen and an open source window manager called Enlightenment. 

Terminology is a new(ish) terminal emulator designed to work on top of Enlightenment. But you can use it with any Linux window manager, if you want to. Right now I'm using Terminology in combination with Xmonad -- a tiling window manager.

To install Terminology on Arch Linux, type

	$ sudo pacman -S terminology

This will install a stable version of Terminology and EFL. You don't need to install Enlightenment, but you can if you want to try it out.

To get the latest version use [cower](https://bbs.archlinux.org/viewtopic.php?id=97137) to grab the AUR package 

	$ cower -d terminology-git
	$ makepkg -si

This is how Terminology looks when you launch it in Xmonad. 

<img src="/images/terminology.png" class="full profile" />

There are a couple of other themes for Terminology. The default theme...

<img src="/images/terminology2.png" class="full profile" />

Notice the blue cursor, instead of white.

And, of course, Solarized...

<img src="/images/terminology3.png" class="full profile" />

To access these themes, you'll need to three-finger-click on Terminology. If your computer doesn't have a dynamic trackpad, then try using your middle mouse button to click.

<img src="/images/terminology4.png" class="full profile" />

### Benefits of Terminology

There are a number of reasons why you'd want to use to Terminology over xterm or another older/standard terminal emulator.

You can use the command

	$ tycat evbogue.jpg

to print a photo to terminology.

<img src="/images/terminology5.png" class="full profile" />

And, as an added bonus, all links are clickable in Terminology. So when you get a link sent to you in irssi, you can just click to bring it up in your default browser.

There are of course annoyances. To turn off the typewriter sound, open up the settings and unclick 'visual bell' in Settings/Behavior.

There are many other Terminology features. [Watch the video by Raster](http://enlightenment.org/p.php?p=about/terminology&l=en) to see many of them in action.

