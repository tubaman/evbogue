---
title: If the Singularity is centralized, we're dead
date: 2014-06-20
---

There's a popular idea among people who are involved in tech, that in 24 years or so a "technological singularity" will come and sweep away all of the human race's foibles and whisk us to a new era where no one ever dies.

The Singularity idea was invented by John von Neumann (who, fortunately or un, did not make it to the Singularity), popularized by sci-fi writer Vernor Vinge, and proselytized by Ray Kurzweil. 

The Singularity seems cool in theory. I wouldn't mind being able to live forever, if I want to, and explore the entire universe while having sex with other immortal bi-sexual polyamerous cyborgs. 

The trouble is all signs point to a centralized closed-sourced Singularity showing up instead of one that liberates the human race. Most people used centralized closed source, and often manipulative technology. If The Singularity arrives during the present state of things, Cyborg Jesus will no doubt enslave the human race -- and in the worst case wipe us all off the face of the universe.

Why would Cyborg Jesus want all of us humans erased from the spacetime continuum? I have a two ideas.

1. Most of us are dumb enough to be enslaved by a centralized program already. I mean, look around you, everyone is filled with blue goo. Maybe cyborg jesus already arrived, and he's Dark Shmuckerberg. Your brains already belong to him. If you don't think so, prove it by taking a break from the blue box for five minutes, a week, a month. I don't think you can. What makes you think a Singularity will liberate you, if you can't liberate your own ass from a stupid blue program that's taken over your life for the past ten years? 
2. Ray Kurzweil works for the most centralized and second worst company on the planet. If the Singularity is born out of Poogle, it's mantra will be "if you have done nothing wrong, you have nothing to hide." and thus your enslavement will begin as every inch of privacy you thought you had melts away as Cyborg Jesus indexes your every pee. 

Yes, I am accusing Ray Kurzweil of working towards bringing about the Singulapocolypse. I realize paychecks aren't as fat when you're working towards liberating the human race. But come on Ray, do you really think an AI born out of Poogle's greed won't erase your imperfect ass from the planet? You'll be the first one in line for the new life that awaits you on an offworld colony as the AI you built brings order to the universe. 

If people continue to use technology along the current trejectory and a Singularity occurs it will lead to the destruction or enslavement of the entire human race.

The only alternative is to begin using technology in a distributed way. We need to see and edit the source code. We must not continue to be abused by centralized algorithsm. We must be able to continue our own destiny as we enter The Singularity.

Because if the Singularity is centralized, we are all dead. 


