---
title: How to livejournal without Livejournal
date: 2014-05-14
---

I've decided to dedicate this blog, for the time being, to the lost art of livejournaling.

Livejournal itself was long ago snatched away from us by The Russians. Who also took Crimea away from John Kerry's terrible plans to rape and pillage it.

Maybe it's good that Livejournal isn't in America anymore, because they can't send national security letters to it.

But the spirit of Livejournal is still alive in these here digital halls. You must summon the spirit of Livejournal to write in a particular way. 

In the olden days of 1999, this was called opening up a vein. You cut yourself and then spew whatever drops into your mind onto the innocent pages of the Internet.

Now the Internet isn't so innocent anymore. It's been assfucked for ten years by Mark Zuckerbergs tiny weewee, and all of the people who choose to suck on it. This only makes me more bitter, which is perfect for livejournaling. Which is what I am doing right now.

Dear Internet, I'm drinking an entire bottle of Syrah/Malbec from Mendoza, Argentina. I want to write to you and ask the following question...

Will I be more or less likely to be nuked if I move to Mendoza, Argentina?

The answer, of course, is I will be less likely to be nuked. Now I can patch up this vein and get on with things. The oracle has spoken, and I am only halfway done with my first two sips. No one wants to nuke South America, because it's not a tyranical empire bent on starting WWIII with Russia.

South America, as far as I can tell, minds it's own business. Except during futball games, and that one time when they tried to reclaim an island from the British.

When I was first livejournaling, there weren't 50 million people in America on foodstamps. Foodstamps were for people who were poor, not everyone.

I almost got on foodstamps once. My roommate was on them when I live in Portlandia in 2009. I even went and got the paperwork. Then I realize if I signed these papers, I would be my governments' bitch. So I turned my back on socialistic bullshit forever. Now look at me, even more bitter than I was five years ago. But at least I make enough honest money to feed myself and pay for my 5000 peso apartment. 

I went to the fish distributor today and I got an entire bag of shrimpies to feed to my bettas for only $10 pesos. Now they're swimming around my fridge in frantic circles, similar to your mind on facecrack. On the way to the pez store an American walked by and he said 'like' seventeen times in the 30 seconds it took for him to cross in front of me. Every, like, other, like, fucking, like, word. 

It's not 'like' something, dumbass. It IS something. If you can't commit to it being something, then you should fly your ass back up to Los Estados Unidos and wait until I'm elected president, and I ban the fucking word 'like' by executive order. I know, it's censorship. Censorship of dumbassery. I will mandate that Facejunkies be forced to hit a button called 'fart'. So they know how important the work they're doing for Zuck is. Fart important.

Alright, I'm through with my first cup of Mendoza Syrah/Malbec. Three bottles for $199 pesos. Time for cup numero dos. What will I say to piss you off next?

***

I'm back with my second cup of Syrah/Malbec. 

To livejournal without Livejournal you must be more honest and depressing than a Aronofsky film. The person on the other end must puke more times than you during the next week as they attempt to wipe what they read from their mind. 

After reading, your reader should email you right away and demand you get on anti-depressants. You of course should reply "I'd rather slit my wrists than get on anti-depressants." And then your reader will not email you back ever again. Back to Facecrack for them, where talk of wrist slitting is always reported to law enforcement by the algorithm. 

I read the wikipedia page on Joseph Stalin. It reads identical to Obama's time in office. Leaps and bounds in healthcare and literacy, but 25% of the population is homeless and 40% of the population has no income at all. Is this a triumph?

I noticed a guy at the fish store who printed off a photo of Hitler, and another of a swastika. I'm not sure if he has them in his fish store for the irony, or he's a neo-nazi. I did not ask. Always mistaken for being Alemán in Mexico, I am not allowed to ask such questions. Nor is anyone who lives in Alemania. Hilter didn't exist and Stalin didn't do to Russia what Obama is doing to los Estados Unidos.

I am sure to win this presidential bid, with talk such as this. Don't make me run! I will kick Hillary's ass. I'll email Ron Paul and ask him to endorse me over his own son, because I will be a better dictator. I will listen to no one. Not even Dick, if his heart doesn't stop beating before my name ends up on the secret censorship blocklists.

I've been less responsive to emails than I should. Somehow it seems better to write everyone than to write one person at a time.

The act of livejournaling without Livejournal is to practice guaranteeing your right to free speech. Because if you can write to the Internet, and no one can stop you, then you will continue to live in a free world. But if you continue to pass every single word through the government sanctioned censorship filters, then you will always live in fear that your update might not end up in someone else's stream. This is a safe, and unsatisfying life. You can live one of those. 

I'll choose to continue livejournaling without Livejournal.
