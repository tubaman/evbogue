---
title: Get off social, keep your website, and go back to Mexico (what I'd tell myself two years ago) 
date: 2014-06-18
---

Have you ever wished you could go back in time and tell yourself to do something different with your life?

My biggest regret is not dropping out of college (as I did with highschool and kindergarten) the same year Lady Gaga did. I wanted to, but everyone told me not to, so I stuck with the herd. But that was 9 years ago, so it's impossible to say what could have happened. I could have been the next Lady Gaga! Or I could have ended up homeless on the streets of NYC without a diploma to wipe my ass with.

So we'll let that mistake stay in the past.

But what about two years ago? That's not too far to go back and change history a little bit, right?

I made a series of dumb decisions in 2012, so I want to tell you what they are, and how I could have avoided them. Then, if you're making similar dumb decisions now you can avoid the same fate.

1. Get off Google+. In 2012, I was already off most social networks. However I held out hope that Google's 'don't be evil' mantra had some play. Now it's obvious that Google's social network failed, and because I trusted them with the majority of my content, my business failed in 2012 with them. Social networks only want to monetize users, they don't want you to monetize them. Anyone who tells you they're making 'huge profits!' on social are lying. Ask them for pay stubs.
1. Keep a website. The critical mistake I made was outsourcing my web hosting to Google during this time. How many people do you know now who don't even have a website? Just a social network page. Well, from my perspective these people don't exist for me. And when I was just using social networking, I didn't exist for anyone else. I was just another item in the stream to be skipped over on the way to #Caterday.
1. Don't go to Singapore. In 2012 I jumped on a series of very expensive flights out to Asia. Why? Because Singapore has nice brochures and an un-free press. There is NO negative press about Singapore because the people who write negative press about Singapore find themselves in the same place all of the homeless people in Singapore went... The meat grinder? Singapore might appear to be a free place from the Internet, but don't let the fancy brochures fool you. The people there live in socialist housing provided on the condition that they never ask questions about anything to anyone.
1. Go back to Mexico. You were just in Mexico! You had a great time! Go back. The reason you had a great time is because Mexico is a developing free country where almost everyone makes money or has a job. Bonus for it not being a totalitarian regime.
1. The only way you'll compete with Barack is by getting hella political. The 2012 election, in addition to the Google+ idiocy, destroyed my business. Everyone was 'Barack Barack Barack!' on the Internet. Including the entire staff of the US State Department's propaganda department. When everyone is thinking about politics, the only way to counteract that is to engage. Instead of pretending politics doesn't exist, get so political that people can't stop thinking of you when they look at the dumb look on Barack's face. He's everywhere, take advantage of that by being in opposition to his dictatorship.
1. Put Linux on your machine. You can't look at that Macbook Air the same way now that Steve is dead and Tim Cook is stearing Apple into the shitter. You put Linux on your machine in October 2012. Instead of waiting that long, put Linux on your machine in January 2012. It'll blow your mind and open new possibilities.


