---
title: Gmail embargo
date: 2014-02-07
---

Effective March 1st 2014, I will no longer answer messages sent from Gmail accounts. I also will begin removing readers from my list who continue to use Gmail.

Why? Google has betrayed the trust of its users by punting all of their information to the NSA. Google makes all of their profits from invading human privacy. 

Google has lost much of the trust they built, and are also eroding the distributed architecture of the Internet. 

When people use Google, they are undermining their own freedoms, and assisting the on-going destruction of a free and open Internet.

When I respond to someone who's using a Gmail account, I have no choice but to interface with their system. My email is archived forever, because of you. I no longer want this to be the case.

I do not have a Google account. I do not visit Google websites, or use their services. It's fine to not have one. You will not die.

So, effective March 1st 2014, I will no longer answer messages sent from Gmail accounts.

Please find an alternative way to get in touch with me. If you claim to be hardcore, set up your own email server. If you don't have any tech skills at all, choose another free email service provider. You can message me on Twister, or Bitmessage. You can join my IRC channel and leave me messages or chat with me in real time. Or pay for email somewhere other than Google. 

You can reach me, just don't use Gmail to do it.

I realize email is not private, or safe, or secure. However, when you use Google you are an active participent in destroying your own freedoms.


