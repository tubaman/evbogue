---
title: Practical Transhumanism
date: 2014-01-14
---

I'm not against selling copies of my old works to clients. So when a reader asked me if he could buy Augmented Humanity, I said 'sure, $35!'

For those who missed February - April 2011, Augmented Humanity was a transhumanist philosophy book I wrote during this period. 

I asked this reader what he thought of the read and he said that he would have appreciated more practical useful ways to approach transhumanism. I can see why he would want this, as Augmented Humanity was the most impractical book I ever wrote.

Before we learn about practical transhumanism, let's first find out **what is transhumanism?** 

There are people in the world who believe that humans will, at some point in the near or distant future, merge with machines and live forever. These people are either called Transhumanists or Singularitans. They tend to follow either Ray Kurzweil or Kevin Kelly -- depending on what they believe technology wants. These people may or may not be religious nutsos, depending on whether or not their predictions come true. 

Transhumanists are not to be confused with transsexuals. Instead of having boy and girl parts at the same time, transhumanists either want to or do have both human and machine parts at the same time.

The ultimate cake for transhumanists is, of course, immortality. Similar to The Fountain, but with more cybernetic implants. Once they achieve immortality, these cyborgs will get to experience just how dull and boring the entire universe is as they explore it with their digitized minds until they are ten million centuries old.

Transhumanists are known for watching Transcendent Man, the documentary about Ray Kurzweil that gives a pretty clear and unbiased picture of the whole thing. You get to decide, do you believe the hype about exponential curves, or do you get your papers in order knowing that you're going to die one or seventy-five years from now -- and no technological innovation will save you.

I used to be a hardcore believer in The Singularity. But I stopped believing the closer I got to understanding what actually goes on inside a computer. This leads me to believe, from personal experience, that most (if not all) transhumanists don't understand their stack enough to know there's nothing super intelligent going on down there. Also, Ray-K got a job at what I think is [the most horrible company on the planet](/googleharmful). That disenfranchised me from the whole 'true believer' thing. 

I'm still a hardcore believer that computers are the most amazing invention of the past 100 years. I just don't think we're going to be immortal super-cyborgs anytime soon.

### Practical transhumanism

The only way I will believe in transhumanism again is if transhumanists prove me wrong. The only way this is going to happen is if some of them augment their humanity in a way other than logging into a Poogle or Facecrack account.

This is where practical transhumanism comes in. When you're a practical transhumanist, you don't buy the bullshit unless you know from real world experience that it is true. 

The best way to become a practical transhumanist is to get to know your computer.

This means installing Linux. 
This means learning to code. 
This means [building your perfect machine](http://build.evbogue.com).

The deeper you get into your stack, the more you'll know about how computers work, and the less you will be taken up with the hype of believing computers will make you live forever.

If you disagree with me, prove me wrong! Bring about The Singularity, I'll come along. 

Until y'all are living forever, I'm not buying it.
