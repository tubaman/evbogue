---
title: Abstractions in computing
date: 2014-06-14
---

The physicality of computing is obvious. You have a computer, it sits in your lap, it's a laptop. Inside there's a processor and memory. Mine has two hard drives, one 20GB SSD and a 500GB spinner. Then there's a screen, and a microphone. And the camera that everyone puts tape over nowadays so the fuckwads at the NSA don't upload their boobs to John Kerry's laptop for his viewing pleasure. 

Oh yes, and let's not forget the keyboard, trackpad, and USB/HDMI/other ports. 

The physicality of your laptop stops there. The rest of your computer is an abstract universe of programs, folders, and files. These abstractions are programmed in whatever language they need to be programmed in, and then converted to machine code, run through the processor to make the abstractions come to life on your screen.

Because most of your computer is abstract, the most known programs are the ones that bring everything onto your screen. Anyone who has used Mac OS X or an iPhone didn't have to use any sort of abstract knowledge of a computer to pilot these systems. You just click on the buttons and stuff happens. This is why when you got a Mac, back when it was cool to buy a Mac, it came with the Dock preloaded with all kinds of shit you didn't need. All of the shit in the dock gave you things to do that made you feel as if you were using a computer.

But the trouble with computers is the more graphical the interface gets, the less abstract it is made, the less useful it becomes. 

Also, the dumber your target market becomes, the more you need to protect all of the underlying computing from idiocy. That explains why Mac OS X is a useless piece of locked down graphical garbage now. Now that Macs are mostly bought by Chinese people and grandmothers, they have to be idiot proof otherwise people won't feel like Geniuses when they're using dumb technology.

Over the past few years it has become clear you can't do interesting things on a computer if you're trying to avoid abstractions.

You can't make great work with the paint-by-number applications that came on locked down/controlled systems.

This is where [building your perfect machine](http://build.evbogue.com) comes in. 

When you're using Linux. Let's say Arch Linux, because it's the best. You get the opportunity to build your machine from scratch. 

Once you get the kernel to take, you start off with a command line and a package manager. 

In the beginning there was, and still is, the command line.

The question is, what are you going to do with it?




