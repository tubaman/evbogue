---
title: A brief guide to modern blogging
published: 2013-12-10
---

+ So you want to be a blogger? 
+ You want people to read your writing, and buy your digital products? 
+ You want to extend your influence over the planet through the words you type into Vim? 

You've come to the right place. Many blogging guides, at this point, are out of date and deceptive. We're nearing the end of 2013, and the blogosphere as changed. In some ways, for the better. In other ways, you're at a severe disadvantage if you're starting out today for the first time. 

In this piece, we'll cover the current state of blogging, and how to get involved as fast as possible -- without falling into some of the common blogging traps.

The goal is to give you an overview of the blogging landscape. Perhaps you'll use this information to become a successful blogging superstar. Or more likely you'll ignore me, and keep doing exactly what you're doing with no regard to my perspective. Whichever your choice, you're free to continue reading.

#### Blogging dos and don'ts.

Do: 

+ Use a version control system
+ Self-host your blog
+ Use a static website generator
+ Write in markdown
+ Learn some code
+ Design your own blog

Don'ts:

+ Use centralized blogging platforms
+ Use a database
+ Track traffic, instead track sales
+ Call it a blog
+ Use Wordpress

That's your list. Write it down into your fancy notebook, or tape it on a wall, because it's the definitive list. 

Who am I to tell you the dos and don'ts of modern blogging? I'll keep the credentials short: I've been doing this a long time. I make my entire living doing it. I'm able to travel the entire world doing so. 

Below I'll break down the dos into specific chunks, so you don't misunderstand me at all. For your added pleasure, I've scattered explanations of the don'ts below as well. 

If you want to keep reading, you can. If you don't want to keep reading, you can convince your mind we never met and go on with your life. No hard feelings.

#### Use a version control system

If you're not using Git to maintain your modern blog, you're living in the past. This might be the recent past, but it is the past. Using a version control system helps you track changes to your blog through time, and it also keeps you from losing all of your content because your database gets infected with malware. The biggest barrier to using a version control system to maintain your blog is ignorance and procrastination. You don't know how to use Git, so you continue to not learn for years and years, and meanwhile everyone else around you has learned it and you're left out in the cold.

Using a version control system with your blog is a great modern way to maintain your blog. But the only way to start using Git to maintain your blog is to use Git to maintain your blog.

The bonus if this is you don't have to use a database anymore. If you're not a database expert (who is, really?) then using a database is along the lines of trusting a black hole to keep track of your valuable shit. Black holes just can't be trusted. Git, on the other hand, is a very transparent way to see all of the changes to your blog and blog content all in one go! 

*To do* First learn Git. Then, start using it to track changes in your blog content.

#### Self-host your blog

Too many once powerful blogging writers have gone the way of the dinosaur because they relied on someone else to maintain their blogging platform. You need to learn how to deploy your own blog, and not rely on the centralized blogging fad of the moment. 

Don't use centralized blogging platforms, they are a fake out. They want to make money off you, instead of you making money on the Internet. Eventually all centralized blogging platforms will turn evil or shut down. Avoid this fate by hosting your own blog.

Yes, you will need to learn more about code and the Internet. However, no one has ever become a blogging success being ignorant of their own codebase. Get into the code now, and in the long run you will be better off. Are you in this for the short term? Then go use a centralized site until they fuck you or shut down.

#### Write in markdown.

In the olden days people used CMSes to write blogs. Now, we use markdown. Markdown is a fun way to write html without all of the brackets. 

There are two big benefits to markdown. 1. It's portable. 2. It's easy to write with any editor.

#### Learn to code and design your blog

The Internet is a global network of computers. To commicate with computers, you need to learn to code. While you might be tempted to outsource all of the hard technical stuff to someone else, eventually this will come back to haunt you. Start learning to code now, and integrate this coding knowledge with your writing/blogging knowledge. Not only will you appear to be more savvy on the Internet, you also won't be left all alone two years from now with a blogging platform someone else built for you.

Start by learning some basic web design best practices, as well as HTML5 and CSS3. As I wrote in [Design Your Website](http://design.evbogue.com), the best person to design your website is you. Hiring a web designer will only cost you money, and you probably won't get what you want. The reason for this is easy to spot, you are the best judge of how your website should be designed. This is cheaper for you, easier on you, and will also teach you digital self-reliance. When something goes wrong with your blog, you will know how to fix it, because you designed it yourself.

Learning to code will teach you the mechanics of how information travels across the Internet.

Learning to design will teach you how to spot trends as they travel across the planet.

Being a savvy blogger is about understanding these two things, and learning to write at the same time. If you try to get by without knowing one, or the other, you will eventually be stuck with a blog no one wants to visit hosting a whole lot of content no one wants to read. If you keep up with the times, and use advanced systems to keep track of your work, then your writing has the potential to reach a lot of people and last a long time. 

Reach and longevity are the two cornerstones of a successful product and a money-making blog.
