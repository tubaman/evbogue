---
title: About the redesign
date: 2014-06-13
---

At some point in the middle of May I started to get overwhelmed with the amount of content I had on evbogue.com. I'd been writing for nearly a year a few times a week, and I'd accumulated quite a lot. As most blogs do.

I can't tell you how many times I go to a blog and have no idea how to sort through the content. I always bring up bloggers who were famous in 1999, but are now obscure and only remembered by people who were blogging in 1999. 

Such as, umm, me. 

Some of these guys have so much content it's impossible to know what is good and what is not. I get the feeling they keep all of the content around because they think it will give them more credability. But instead they appear to be digital hoarders.

No dobut they think they're minimalists, because everyone thinks they're a minimalist. Including people with 135,654,232 blog posts dating back to 1999. 

Someone once pointed me to an article at the w3c telling me that it was moral obligation to never allow linkrot to happen on the Internet. 

Well, I say fuck that. Let the links rot. If you never throw anything away, how will you ever know what's good in your life? Also, how the hell with you find it in that great big pile of junk?

I want my website to be as the junk my life is, all fitting into a neat tidy package. Even if everyone hates me for it.

Besides, people only read the latest post and one or two other anyway before they decide to buy something.

The fact that not all blog posts are good. I'm quite sure every blog post I write is terrible. If it isn't terrible right now, I will consider it terrible in the future. People who think every blog post they shit is gold are sitting themselves. If every blog post you shit is gold, why are you on foodstamps?

If you were visiting this blog a month or so ago, you will see that the blog I have now is very similar to the one I had then. Except for one very subtle change. There is no archive.

I've selected five top posts, recommended by you a week ago when I asked what you remembered as my best posts from the past year. Some of you will be sad that your favorite post was not included, however I only had room for 5. 

I realize doing this is a form of self-control. No one has any of that anymore. You can't have self-control when your dopemine center has been fingered by Zuk. However no such rules apply to algorithm-free me. So I can do whatever I want. 

I might not be a creative genius, but I have figured out how to at least make the blog managable by my own standards. Less mental debt means more creative freedom. For me anyway. I'm sure if you ask any blogging who has an archive going back to 1999 that they get oodles of inspiration while spending 27 hours a week re-reading their glorydays.

Me, I'll focus on this moment. This post.

I didn't want to get too carried away with the designing, being that I'm shelping a limited time product called [Undesign the Web](http://undesign.evbogue.com) right now. I hope it's undesigned enough for you to voice your approval. And if you hate it, please let me know too. I will argue with you until my last breath. If you still do know how to argue, which no one seems to do anymore.

I did add something that may go away. It depends on how I feel about it. I have 5 or less outbound links. I've noticed that I feel compelled to write about world affairs right now. What with the US trying to start WWIII with Russia, only to find it appears to the entire world that they dropped the ball on the phony war they started 11 years ago in Iraq. Instead of writing so much about it, I figure I'll select a few stories that you won't find covered in mainstream media. This way you won't be in the dark when Russian bombers decide to fly closer to California than 50 miles. 

Whatever happens, it is/was John Kerry's fault.



