---
title: What's the difference between Undesign and Design?
date: 2014-06-04
---

The questions are coming.

I dropped a product into the world a few days ago, [Undesign Your Website](http://undesign.evbogue.com), and the first question is an obvious one.

"I bought Design Your Website a few weeks ago, what's the difference between Undesign and Design Your Website?"

First of all, Design Your Website isn't gone. [It's over here](http://design.evbogue.com). The reason I took it off the front page was because I don't want a paradox of choice to appear because I have two design products at the same time.

Don't worry! Undesign is a limited time offering, so I'll stop selling it on June 24th 2014. In the meantime you can choose to pick up Design Your Website and read it at the same time, or wait until later, or just buy Undesign. In fact, if you do NOT know HTML and CSS, you should probably buy Design first.

Design Your Website is the strongest book I've ever written because I know it so well. I've been designing websites since I was 12, and I'm going to be 30 next year. Do the math. 

Another person asked "why should I buy Design Your Website if I could get a free course and such-and-such.com?" That's obvious, because I wrote it and their free course is crap. The reason I wrote Design Your Website was because I had zero decent resources to recommend to anyone, and almost no one knows how to code in HTML anymore. It's an art that's been lost to ten years of Zuksucking. Everyone feels 100% deballed when they attempt to use the distributed Internet and can't even manage to type some basic HTML.

Now Undesign isn't about basic HTML, while there is some of that too. It's aimed at the people who read Design Your Website and want to take what they've learned to the next theoretical level. It's also aimed at all of the people who skipped Design Your Website because they've developed a decent understanding of HTML/CSS in the past. 

Undesign the Web is more of a theoretical rant than a step-by-step guide to learning HTML/CSS. I've been pissed off by all of the recent redesigns of things to look worse than they did before. Everyone is attempting to go all Web 2.0, but Web 2.0 has been passe for two years. What we want now is Undesign. We want clean clear lines. We want markdown rendered into HTML. We want to take those social web buttons that spy on us and throw them in the digital trash can. We want Undesign. 

That's why I'm writing [Undesign the Web](http://undesign.evbogue.com).

I wrote [Design Your Website](http://design.evbogue.com) because people were asking me how to learn HTML and CSS and I had no decent resources to recommend.

Clear or no? If no, contact me with questions!

