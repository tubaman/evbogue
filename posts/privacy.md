---
title: Privacy
date: 2014-07-04
---

I've been trying to figure out what to write tonight. I'm blasting Tom Waits into my earbuds. I'm drinking a Bohemia Obscura. I bought oddles of plants for the betta fish today.

Yesterday I finished working on a book about my current favorite technology, GnuPG. Otherwise known as The GNU Privacy Guard. Otherwise known as keeping the NSA from snooping on your messages without oodles of supercomputing time to crack your cryptography.

The title is, as you might expect, [Pretty Fuckin' Good Privacy](http://gpg.evbogue.com).

I put off learning GPG for a long time. I found GPG difficult to learn. The workflow felt convoluted. 

But then this guy named Snowden released a whole bunch of documents about how the United States government has turned into this authoritarian dictatorship with bad dreams of becoming the dystopia imagined in George Orwell's 1984.

I'd been paranoid about privacy before this moment. I'd left all of the social networks, of course. But everyone told me I was just a 'tin foil hat', and there was no truth to anything I speculated. The government had MY BEST INTERESTS AT HEART. 

Does anyone still think this? In the one year since Snowden released his documents, have they done anything besides saying a handful of words to make the world, and more important, their citizens, feel more comfortable with their plans to gather as much information on everyone in the world as possible? Nope. 

So a year after Snowden, I'm no longer a tin foil hat. I'm just a practical dude who wants a little privacy in my communication. I'm an average boring example of an American citizen who doesn't want anyone snooping on his mail.

Anyway, over the past three weeks, I've learned GnuPG. I took this opportunity to write a detailed guide, so you can learn it too.

You might be thinking "what fucking right do you have to write a book about something right after you learned it?" That's an excellent point.

The way I see it, most of the guides to GnuPG suffer from what I call 'over-familiarity'. They are written by supergeeks who have used the technology since it was born, and they don't understand how fucking hard it is to learn how to use the thing they just take for granted.

My idea with [Pretty Fuckin' Good Privacy](http://gpg.evbogue.com/) was to write the guide while it was fresh in my mind.

I ran the new book by everyone in my audience who knows GPG, and it passed the test. It's ready for public consumption. The question is, do you want to learn how to encrypt your messages so the NSA can't read them?

Bitmessage was a good step in the right direction. In fact, I wrote a chapter about how to use GPG and Bitmessage together in Pretty Fuckin' Good Privacy. Bitmessage hasn't had a security audit yet, so it's unclear whether or not the technology works or if it leaks your data all over the Internet. The good news with Bitmessage is it obscures your metadata, so when used in conjunction with GPG you can maintain most of your anonymity.

The biggest question about releasing a product like this is obvious: does anyone give a shit? Maybe we're content to live in a surveillience state. 

I don't believe this, and I don't think you do either. Judging by the reaction to the news today that the NSA is collecting records on anyone who visits The Tor Project's website, I can tell almost everyone is on my side. Unless you pick up a paycheck from the federal government, you want privacy and you don't think the government should have the right to spy on you.

Some people think this a political problem. We must lobby for the government to stop spying on us! I don't agree. I think it's a technological problem. We should make it so fucking hard for the government to spy on us that they will give up and go "get a job" flipping burgers or working at Walmart. Then we will enter a new age of freedom due to cryptography.

I realize this is a lofty dream. But I think it's a dream worth dreaming.

Anyway, I wrote a new book. It's called [Pretty Fuckin' Good Privacy](http://gpg.evbogue.com). It's available now.

