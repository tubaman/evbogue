---
title: How to choose a Linux distribution
date: 2014-06-17
---

Intro: I should be a responsible publisher and write more about [Undesign the Web](http://undesign.evbogue.com), being that it's a limited time offering and I'll be taking the sales page down in a few days. Instead, I want to dork out on Linux. Why? Because I think Linux is amazing and everyone should be [building their perfect machine](http://build.evbogue.com). I've been asked by a number of people to cover some Linux basics in BYPM, so why not start with how to choose a distro? The post below will be on this website for the next five days, and then you'll be able to buy it in [BYPM](http://build.evbogue.com).

### How to choose a Linux distribution

One of the most important decisions you'll have to make before you install Linux is which Linux distribution to install?

There are hundreds, maybe even thousands of different distributions. You're probably familiar with Ubuntu, Debian, Fedora, and Arch Linux. But do you know about Slackware, Crunchbang, Linux Mint, Kubuntu, Lubuntu, Xubuntu, Gentoo, Archbang, Manjaro, or openSUSE?

Picking your first Linux distro can be overwhelming. The good news is you are safe to stick with the list of Linux distributions that you already have in your head. Many Linux distributions are obscure and unmaintained. Your first decision should be to pick a Linux distro in active development. Your next decision will be to pick the one that suits you the best.

The most important difference between distributions is how they handle packages. So your distro decision revolves around which package manager you want to use.

Debian and Ubuntu use a package manager called Apt-Get

Fedora uses Yum

Arch Linux uses Pacman and Aur

Then there is a large list of X-based distros that use the same package manager as the distro they are based on, but come pre-configured in different ways. The popular distribution Crunchbang is Debian-based, which means it uses the same package manager as Debian -- but instead of installing Gnome by default, the Crunchbang developers have opted to install Openbox instead.

Let's take a moment to learn how each of the different package managers works. Here is how you can install the popular terminal-based text editor Vim on each of these systems.

Debian and Ubuntu use the same package manager, so you'll use the same command on both

	apt-get install vim

On Fedora you'll use yum

	yum install vim-enhanced

On Arch Linux you'll use pacman

	pacman -S vim

As you can see, each distro has it's own different approach to installing packages.

The other difference is that most distros come with a large set of programs already installed. On Ubuntu you'll get a very opinionated system full of software that's been chosen for you, whereas with Arch Linux your system will only come with the bare essentials. This gives you the opportunity to build your perfect machine from scratch, whereas with Ubuntu you'll have to rip out many guts of the system to get your perfect setup.

I use Arch Linux because I'm an obsessive minimalist who wants as only the applications on my system that I installed myself. I don't want anyone to make any decisions for me. However you might feel more comfortable choosing a distribution that comes pre-installed with a lot of software, then you can experiment with the software until you get comfortable enough to move on to Arch Linux to build your perfect machine from scratch.

### How to choose a window manager or desktop environment

The next decision you'll need to make is which window manager or desktop environement to choose. A desktop environment is a complete graphical experience, whereas a window manager is more focused on keeping track of your windows. If you're familiar with Windows or Mac OS X, both of these systems only have one window manager that comes installed. You cannot change the window manager on these systems.

However, on Linux, you get almost infinite possabilities when choosing a window manager. 

Arch Linux doesn't come with a window manager pre-installed, so you get to choose the one you want. I always install Xmonad, which is a tiling window manager.

Ubuntu comes with a very opinionated desktop environment called Unity. 

<img src="http://i1-news.softpedia-static.com/images/news2/Ubuntu-14-04-LTS-Trusty-Tahr-Arrives-on-April-17-Three-Features-to-Look-For-437402-3.jpg" class="profile full" />

Debian comes with Gnome installed.

<img src="http://www.neowin.net/images/uploaded/debian.jpg" class="profile full" />

Fedora will ask you if you want to install Gnome, KDE or XFCE.

<img src="http://fedoraproject.org/w/uploads/5/59/Gnome_Classic_Places.png" class="profile full" />

Crunchbang comes with Openbox installed.

<img src="http://1.bp.blogspot.com/-Uoq_9wXME54/UZP385svbAI/AAAAAAAACCA/TOodYID115c/s1600/Screenshot+-+05152013+-+08:04:35+PM.png" class="profile full" />

The other thing worth noting is you can install whatever window manager you want on whatever system you want. Part of learning how to build your perfect machine is trying as many window managers as possible until you find the one you love the most. This will no doubt keep you busy for days, weeks, maybe even months as you try all of the different options out before you settle on the one you love the most.

Me, I'm settled into Xmonad. Because I can't imagine dragging windows around anymore. Once you go tiling, it's pretty hard to go back. You don't know how much mental energy you waste dragging windows around all day on window managers that make you drag windows around all day.

<img src="/static/xmonad.png" class="profile full" />

For awhile I used a somewhat obscure desktop environment called Enlightenment which has a lot of chromatic eyecandy.

<img src="http://1.bp.blogspot.com/-wJoM__c2y0k/UrdNRMTaPxI/AAAAAAAAGvQ/rN5_vUL2Zqg/s1000/enlightenment-1.jpg" class="profile full" />

### Should you install Arch Linux or not?

The real decision comes down to whether you want someone to think for you or if you want to think for yourself. Most Linux distros will come with a set of pre-determined applications that are already installed on the system. Then if you want to add some applications, or take others a way, you are free to do this.

Most of Build Your Perfect Machine is about using and customizing Arch Linux. The reason I've decided to focus on Arch Linux is because I think it's the best distro. I've tried them all, and Arch is best for me. Maybe it's best for you? Also, Arch Linux is the biggest challenge. These days, with computing all dumbed down to iPhones and Androids, it can be very empowering to make all of your own decisions and build your perfect machine.

This is why I've choosen to focus the rest of this book on Arch Linux. If you need to install a different distro and play around with it in the meantime, you do that. Eventually you'll come back to this book because you'll want a perfect machine, and the only way to get a perfect machine is to install and build using Arch Linux.

You can buy [Build Your Perfect Machine](http://build.evbogue.com) using the button below. It's $37.

<a href="http://evbogue.fetchapp.com/sell/dyiwoove"><button>Buy Now</button></a>
