---
title: How to choose your first Linux distro 
date: 2014-01-10
---

2014 is the year you'll switch to Linux.

Here's why I know...

Over the past two years, Linux became the most popular operating system on the planet. Linux is ready for your destop or laptop computer. Linux is stable, it's open, and more important, it's (probably) not spying on you.

But which Linux do you start with...? 

There are hundreds of Linux distributions. Some are more known, or popular, than others. 

When I started using Linux in 2012, Ubuntu was the Linux distribution of choice. Over the past few years, it's fallen out of favor. Now I use Arch Linux to [build my perfect machine](http://build.evbogue.com).

Let's talk about the pros and cons of the four most popular Linux distros below. In my opinion, ordered from best to worst.

**Arch Linux**

[Arch Linux](http://archlinux.org) favors simplicity over everything. You get to choose how you want to build out your machine from scratch. Even if you start with one of the other distros below, you'll probably end up using Arch as your skill level improves. Because Arch Linux is built on [Systemd](/systemd), you'll have a stable Linux experience. Use Pacman to update your system to the latest of everything, as well as build your system out from scratch any way you want.

Pros: code elegance over user convenience. Cons: medium/high learning curve as you build from base install

**Manjaro**

At the suggestion of [Rmorabia](http://www.rmorabia.com/), I've added [Manjaro](http://manjaro.org). Manjaro is a built out Arch Linux distro. This means you let some other people make your choices for you, and then you get a fully built-out Arch Linux install with minimal effort on your part. **Update** After installing Manjaro, I can say for certain that you're better off with Arch. The install abstraction takes you through the same steps, via drop down menus. Why not learn how to do it yourself? 

Pros: Arch, with attempt to make install easier. Cons: Additional abstraction may complicate install process 

**Fedora** 

[Fedora](http://fedoraproject.org) is the cutting edge distro supported by Red Hat. Many of the coolest things in Arch Linux were pioneered at Fedora. This is why I recommend Fedora to anyone who is terrified of starting out with Arch Linux. When you use Fedora, you'll get used to a very similar workflow to Arch Linux -- but with a graphical installer that will do most of the hard work for you.

Pros: cutting edge, the future of Linux. Helpful community. Cons: high nerd quotient, dorky website

**Debian**

[Debian](http://debian.org) is the hardcore GNU/Linux distro. I say GNU/Linux, because that is what Debian considers itself. There are many Linux distros based on Debian, including Ubuntu. Debian will get you hardcore points for using it. If you want to use Systemd with Debian, you will have to do some jiggery. Debian favors stability over everything, and has around one or two stable releases a year. It's not as cutting edge as Arch, but a lot of nerdy people are testing stuff ahead of time for you.

Pros: hardcore, stable. Cons: slow release schedule

**Ubuntu**

[Ubuntu](http://ubuntu.com) is very cute. It has the orange and purple Unity interface. Ubuntu deserves a lot of credit for bringing Linux into the 21st century. However, they've made some unpopular choices in the past year. No systemd, and the Mir window manager controversy means Ubuntu won't be compatible with the future of Linux.

Also bad news: Ubuntu spies on anything you type into the search bar, and reports it back to Canonical -- the company that creates Ubuntu.

The good news is Ubuntu is probably the easiest Linux distro to install. The Ubuntu live USB/DVD just works, no hitches. If you're used to having all your thinking done for you by the people designing your operating system, Ubuntu may be for you.

Pros: cute interface, easy to install. Cons: poor choices on basic system foundation. Spyware :(

***

These four options should give you a good idea of what's available for you to install. I prefer Arch.

With Arch Linux, you'll learn tons as you [build your perfect machine](http://build.evbogue.com)

-Ev
