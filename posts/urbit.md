---
title: How to Urbit
published: 2013-12-02
---


[Urbit](http://urbit.org) is a nifty piece of technology most people have never heard of. When you use Urbit, it takes you to an alternate dimension of cyberspace where the Web doesn't exist and nothing is compatible with anything invented in cyberspace before the invention of Urbit.

Whaaaat? Yes. Exactly.

In this alternate dimension people (and perhaps someday, Cylons) fly submarines and destroyers through cyberspace over UDP and identify themselves with cryptography.

It's all either completely insane or brilliant. 

The coolest thing about Urbit is it generates unique identifiers for all users (and cylons) so you can send messages to them with the knowledge that they will (perhaps, in an ideal world without continuity breaches) be delivered.

[OFFICIAL DISCLAIMER] Urbit is experimental networking technology. You might run up against problems using the following tutorial because the network is down, or in some state of disarray. Do not despair. To quote the [Urbit tutorial](http://www.urbit.org/2013/08/22/Chapter-1-arvo.html) on Arvo, "Please return to your NSA-certified digital plantation." If you run into any problems, please do not bother me. Instead bother the Urbit people. I provide this tutorial without warranty or customer support of any kind. I just think Urbit is cool, that is all.

To get started with Urbit, please first watch the talk Urbit's creator Curtis Yarvin gave at Rackspace not too long ago. 

<iframe width="420" height="315" src="//www.youtube.com/embed/6S8JFoT6BEM" frameborder="0" allowfullscreen></iframe>

(P.S. Tantek looking so uncomfortable at the start of this video is priceless)

As you might learn from this talk, Urbit is a full stack application for interacting in Urbit. On Urbit you use virtual machine called Arvo, where you can program in a functional programming language by the name of Hoon. Hoon is written in Nock, and Nock is coded in C.

So let's start using Urbit. It should work on your Unix-y system, no matter what incantation. Mac OS X, Linux, BSD, whatevs. It probably won't work on Windows. If it doesn't work on your system, try running it on a VPS using [Digital Ocean](https://www.digitalocean.com/?refcode=26d8ed49730d)! (rewards link)

Clone down Urbit, cd into the directory, and make 

	git clone https://github.com/urbit/urbit.git
	cd urbit
	make

If make doesn't work, you might need to install some developer tools. On Arch Linux, you do this by typing one command

	pacman -S base-devel

On other systems, you'll have to figure it out yourself

To use Urbit, you'll need to create a pier. A pier is where you launch your submarines and destroyers from. Imagine a pier as the starbase connected to your computer where you launch your spaceships into Urbit-space.

To create a pier, you'll first need to set your $URBIT_HOME. To do this, you'll need to do a bit of bash programming. As per the Urbit docs, you'll need to type...

	echo "export URBIT_HOME=`pwd`/urb" >>~/.bash_profile
	source ~/.bash_profile

That should do it, unless you're on a funky system with a different shell. In which case, you should be qualified to update that shell, right?

Let's create our first pier. Navigate yourself to the ~/urbit, or wherever Urbit lives. Now type...

	bin/vere -c yourpiername

You can name your pier whatever you want. Now you should see...

	vere: urbit home is /home/evbogue/urbit/urb
	loom: mapped 1024MB
	loading /home/evbogue/urbit/urb/urbit.pill
	time: ~2013.11.20..02.07.35..3cf8
	ames: on localhost, UDP 53823.
	http: live on 8080
	generating 2048-bit RSA pair...
	ames: czar zod.urbit.org: ip .198.199.112.32
	saving passcode in /home/evbogue/.urbit/~rocsud-samrym.txt
	(for real security, write it down and delete the file...)
	; ~zod |Tianming| is your neighbor
	; ~zod |Tianming| is your neighbor

Great! You've generated your first pier. In a short time, you should see your first submarine.

Mine looks this way, this time

	~tordev-lodtex-ribdyl-lombec--winbec-hacpun-famtul-doznyl/try=>

A submarine is a self-assigned unique identifier you can use to navigate through Urbit-space. Submarines are the lowest of the low spaceships you can generate to fly through Urbit. Submarines are just around to get you a destroyer. Or perhaps to browse without anyone knowing who you are. You can't spam anyone with a submarine. Also try remembering 6 x 6 characters. It's impossible. The good news is, if you play by the rules and are nice to the Urbit people, you might get a destroyer which will give you a 12-character unique identifier, called a Destroyer. 

In a moment, your submarine should download a whole bunch of Hoons. It will look this way...

	+ /~tordev-lodtex-ribdyl-lombec--winbec-hacpun-famtul-doznyl/try/1/lib/diff/hoon
	+ /~tordev-lodtex-ribdyl-lombec--winbec-hacpun-famtul-doznyl/try/1/bin/memory/hoon
	+ /~tordev-lodtex-ribdyl-lombec--winbec-hacpun-famtul-doznyl/try/1/bin/sleep/hoon
	+ /~tordev-lodtex-ribdyl-lombec--winbec-hacpun-famtul-doznyl/try/1/bin/zero/hoon
	+ /~tordev-lodtex-ribdyl-lombec--winbec-hacpun-famtul-doznyl/try/1/bin/overflow/hoon
	+ /~tordev-lodtex-ribdyl-lombec--winbec-hacpun-famtul-doznyl/try/1/bin/reverse/hoon
	+ /~tordev-lodtex-ribdyl-lombec--winbec-hacpun-famtul-doznyl/try/1/bin/fib/hoon
	+ /~tordev-lodtex-ribdyl-lombec--winbec-hacpun-famtul-doznyl/try/1/bin/game/hoon
	+ /~tordev-lodtex-ribdyl-lombec--winbec-hacpun-famtul-doznyl/try/1/bin/goodbye/hoon
	+ /~tordev-lodtex-ribdyl-lombec--winbec-hacpun-famtul-doznyl/try/1/bin/tiff/hoon
	+ /~tordev-lodtex-ribdyl-lombec--winbec-hacpun-famtul-doznyl/try/1/bin/infinite/hoon

When you receive these hoons, you know it's time to email the folks at Tlon (the corporate entity behind Urbit, who are not paying for this tutorial) to get a Destroyer. 

To get a destroyer, email [urbit@urbit.org](mailto:urbit@urbit.org) with your submarine ID in the subject line. Tell them Ev Bogue sent you. Ask nice, and :ye shall receive. 

Once you get a destroyer, it's Urbit party time. 

When/if you get a reply from the Urbit peeps, you should have both a destroyer name and a ticket. It may look this way

	~waclux-tomwyc: ~ribdyr-famtem-larrun-figtyd

This is the stock destroyer/ticket combo from the Urbit arvo tutorial, it probably won't work. Be sure to get your own destroyer, and keep the ticket safe so no one steals your destroyer.

To register your destroyer type this command into /try=>

	:begin

It'll send you this message

	Do you have a ship and a ticket? yes

Hit return

	Your ship: ~navsyn-lacsup

I typed one of my favorite ship names in there.

	Your ticket: ~ribdyr-famtem-larrun-figtyd

Type in your ticket and hit return

If all goes well, you'll be prompted with a series of other questions in regards to your Urbit ship. Answer them to the best of your ability, and at the end you should see

	generating 2048-bit RSA key...
	; ~doznec _Urban Republic_ is your neighbor
	; ~doznec _Urban Republic_ is your neighbor
	request approved

And then after some [Waiting...] your new Hoons should download, and then you'll have a fully operational Urbit destroyer!

	+ /~navsyn-lacsup/try/1/bin/reverse/hoon
	+ /~navsyn-lacsup/try/1/bin/fib/hoon
	+ /~navsyn-lacsup/try/1/bin/game/hoon
	+ /~navsyn-lacsup/try/1/bin/goodbye/hoon
	+ /~navsyn-lacsup/try/1/bin/tiff/hoon
	+ /~navsyn-lacsup/try/1/bin/infinite/hoon
	~navsyn-lacsup/try=>

Now, try the new-ish chat app on Urbit to talk to other urbit-ers.

	~navsyn-lacsup/try=>:chat

Hit return and you should be brought into the world of :chat

	&...
	&

:chat is a hoon app for chatting with other people on Urbit. If there are other people online, give them a shout, perhaps they'll talk to you.

To send a message type into the prompt and hit enter

	& Hello, world!
	&...

If none of this works, don't be alarmed. Urbit is experimental, and may not work for you. Or you might not work for it. Regardless, keep trying. Or go back to your NSA-
certified digital plantation where nothing ever breaks, everything is always perfect, and all your data belongs to someone else. -Ev

Alex wrote [about Urbit](http://alexkrupp.typepad.com/sensemaking/2013/12/a-brief-introduction-to-urbit.html) as well.
