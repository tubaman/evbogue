---
title: Why I switched to Linode
date: 2014-06-16
---

Today I moved all of my Git repos from a Digital Ocean VPS to a Linode VPS and changed my DNS settings.

My website is now being broadcast from a Linode data center in Dallas, TX. 

But why? 

Around a month ago I got word Digital Ocean was dropping support for Arch Linux VPSes. This is too bad, because Arch Linux is the best Linux distro. I use Arch Linux on my computer and my VPS. Arch Linux is the focus of [Build Your Perfect Machine](http://build.evbogue.com). 

So I launched a single-man campaign in attempt to get Digital Ocean to continue to support Arch Linux VPSes. I went back and forth their with customer service (who are very nice by the way), but we couldn't figure out how to get Digital Ocean to change their minds. 

The problem, as much as I could get out of DO, was they couldn't figure out how to update Arch Linux to the latest version and then re-image it. Arch has a rolling release schedule, because it's the cutting edge distro, so there are no 'stable versions'. I love this because I love the cutting edge. 

Finding out that DO wasn't going to change their mind about depricating Arch was too bad, because I'd be recommending Digital Ocean Arch Linux VPSes for over a year. And now they're dropping support for it. I'm a Arch Linux fanatic, so there's no changing my mind.

So I began looking for other options. I asked everyone I knew to weigh in, and the only real suggestion I got was Linode.

Today, Linode announced that they're offering $10 per month VPSes. 

Bonus, Linode has the technical expertise to re-image Arch Linux often enough to offer decent support for it.

So today I switched to Linode. 

The biggest leap for me, with Linode, was that I had to use my credit card on their service. They didn't allow Paypal, as Digital Ocean does. And they don't accept Bitcoins either. But I imagine charging before giving their service keeps the abuse complaints down, so I can't blame them for this.

Since switching earlier today, my ping times are faster. My new VPS pings in at 33ms, and my old VPS pinged in at 79ms. This isn't a huge difference, but it's worth noting.

Otherwise the experience is the same. I had a Arch Linux VPS on Digital Ocean, now I have an Arch Linux VPS on Linode.

The only difference is Digital Ocean was $5 per month and Linode is $10. 

If you want to switch to Linode, feel free to use [this referral code](https://www.linode.com/?r=d214f41a2b60bfc44f594a669446a8a5846bb521) and it'll help fund my $10 per month hosting bill.

Also use the coupon code LINODE10 to get $10 pre-loaded on your account. Not sure how long that one will last.
