---
title: Why you should use your domain for your email
date: 2014-06-19
---

I have a Gitodex (similar to a rolodex, but with Git) full of client names and email addresses. 

Most of these names were exported from Mailchimp when I left on censorship concerns. Others come from people who email me with questions that I want to remain in touch with. 

On occasion I reach out to people to have a conversation and see what they are working on. This helps me figure out what to work on, what will benefit clients in the world, and get a read on where people are at in different parts of the world.

Something I've noticed is I tend to gravitate towards emailing people who have email addresses at their own domain names. 

Take this list of fake email address I just made up

	borg543@gmail.com
	klingon95@outlook.com
	redshirt@hotmail.com
	starbuck@karathrace.com
	picard@jeanluc.com
	seven@cylonoccupation.com

Who on this list do you want to email? starbuck@karathrace.com, of course. 

The other thing I've noticed is I visit the websites of people who use their own domains. I have an impossible time figuring out how to find borg543@gmail.com's website. Most of the time I assume people who use centralized email services don't have websites at all! Most of them don't anyway, so that's a safe assumption.

Here's my request. If you have an you@yourdomain.com email address, can you email me with it or Bitmessage me? You don't even have to say anything thoughtful or ask a question. Just give me a shout and I'll update my Gitodex with your domain name. bonus! You'll get a website visitor.

[ev@evbogue.com](mailto:ev@evbogue.com)


