---
title: GnuPG
date: 2014-06-24
---

I'm experimenting with using GnuPG or GPG over email. I put off learning GPG for a long time because I tend to focus on cutting edge privacy programs. I've had a lot of luck with Bitmessage, however Bitmessage is untested and has some bugs. I hope these bugs will be fixed over time, and that Bitmessage scales to accomidate a whole bunch of users. However, I think it's important to learn how to use GPG because it has been around for a long time and thus most of the security holes in the software have been worked out.

I know a handful of you reading this are geekier than I am, and have been using GPG for years.

The reason I didn't learn GPG right away is I have come across no good documentation on how to use it. Most of the documentation contains massive amounts of assumptions about what users know already. Similar to Linux documentation, the GPG documentation assumes you are a supernerd. The trouble is that sometimes even supernerds like me have trouble understanding how a piece of software operates. GPG is one of these examples. I feel dumb trying to get it to work, and this is not a great sign when it comes to bringing GPG to the common folk of the world. I believe everyone cares about privacy, even if they won't admit it. The reason everyone isn't using GPG is not because they are dumb, but because most of the documentation is not easy to understand and does not take the user down the slot.

The biggest weakness, in my mind, is GPG over email leaks metadata. For anyone who's been following the Snowden stuff, it's clear the NSA thinks they have rights to all of our metadata. So even if my emails are encrypted, it is very clear who I am sending them to and from, unless I find another way to obscure this. Bitmessage, however, does not use metadata at all -- so it solves this problem where GPG does not.

Anyway, I will perhaps write more about GPG in the future if I think it is worth my time. I also want to hear your experiences and opinions of GPG, whether you use it or gave up on it (as I did for more than a year post-Snowden.)

Here is my public key. Feel free to send me encrypted emails using it.

You can also find my public key by asking your keyserver using the command line. Though at this moment I'm not sure if we need to be using the same keyserver or not for this to work.

	gpg --search-keys evbogue

-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG/MacGPG2 v2.0.22 (Darwin)

mQINBFNo7XIBEADMxEVYjIALJhHhZ42bRgF2jU0S7AfkWOAqzdGYasP8j07UN8+O
hPK5wUT5hS3E3lKkFc3P1fGZl6svUexNCq4y7d7M+BhYur+wGVzQVo5Kr5K28htc
bLSWSWW8dsYu7Gv58/2yy+PNl+ENIGvQRIgWwGA86Cu6bJjvAhrd025e7KRPI4KM
gKcpQiXjHWbO3Cf743qvhcPJmGfyVhiPxLxGh1ZsF71edXY4shgZ/A1ZQ1N/8wrl
RckId3nZ3XXBiM9M8SWrohkPlOvdg/73uEFssTyPY88Vt+bH0e4fU71G6Qn+mPhL
bLwakcgSBeHooeQYhRygs4qvVGT5gRaVVkPWe60xJDUXB/Fw97gX+FzANeTQGTSL
1jVOlxxzfZTk1YOk6Qc/cfBsqTxY8zE92AZuN5gVrXCbRW3TwC2+j1rxG92JUH8E
/2ZnsP3CFs7m4ddeQGH4V/oPBrLAWaker30Kb/RFBb0lSkCyRpzGrDMv9DujM21P
XPI/KEc1lHVYSzioe+LEtHMCbP86WNCFwzC0gCAk2mjJcQ4TYWJvRA4Qyx2FdS+j
lQ37Hhe9AiCDENLbrOwXwfwELv8YJnBbKBMmsiw5rz1eXizprv7GgbjbX3kxqIyH
Rwcv5kIrxPTONMmVkAUwbvq/kuxB53HodZpoVd1HRuDk5d8uM9C1Oydx7QARAQAB
tB9TYW15IERpbmRhbmUgPHNhbXlAZGluZGFuZS5jb20+iQI+BBMBAgAoBQJTaO1y
AhsDBQkHhh+ABgsJCAcDAgYVCAIJCgsEFgIDAQIeAQIXgAAKCRAO52YWzFgb4RKo
D/0bH3MJZG95mEggHan9DgawrtN5rDse2usx4CHrp7Tt6TAyYTW3et4ie6hT+Se8
k0kCuH1qCH061DcvVomNQXU7sAbjcQpWDL+gyRUfqmrYc+K1dF4gqKtL7lBduuyD
TpV84cqSUORPvGIkohOn97YiL6QaDHzZFe2FB1YZU7Nf5rKmhyPVngyLNPGBSZxc
E9To8B1ZCok/fHwRhpNUQqTk3fudBQciCR0rSgUnvHWcjSAZfZCTq9crRL1guX24
7w8dpbq0nGa6ZBcMYn/YfvtOSjyGyeSyLiF2y6T8J+nFTjfFjvdpZVYG76/BnYnb
dklBg10dC3zPkv6EJA2NrrJ5KeqAhppKY3T2HRSACr/7YNDsr/R9z+zilTsulBgn
ZZmp6qWQqwzcDbuceZaxdKy/FX51s8M+5SGdfe10ojzxWoxICZvUqgJWHL1tw3Qs
hiQWruH6Lym6kYKg9G3gPMa4dYkRv4Xgk4fzj7DUD2b+g420YOi+w/e+tRq3LC2W
Q9cBcwWMLd46COGvmfLDU11SoANFDVORNWGe2cAyEwDU2FhquEhaAdv1Qj+hjgSN
X+JkmMklMgBsB5o+O2q29qUKzcVB7gaQ1+J/QWgB/mDzgbP3OE4dzRajatxv1YgA
2hTWKo7wZHesesqGHlQYp7C5uh114Kv66OWRegjtDbZuCrkCDQRTaO1yARAAuc++
0qxTfl6jQBAReXLLUZVO6HZLLVJuBmbQYEpqkxKEL/cIaOnsnNHBN/LsusiKg9YB
wVyySueZ3hhaUEEuN6wdD/47YPyR3oFatM32oyGUmSSr5R2dx5+TrwfyfeKpzDmk
1aXzjU3S8nTdJV3pCYBcEKhLAkhcaUT4OGFEko/rt9wUu78ZvTI+0fwjZAe5R9Ot
PmBpDTui4WIPMd6YaAjkeW2jLBlLQlc8BD0eNSofnAFwMhvZnjJmTriskRS5BMII
j+lZjkFFCyIxxo92N1K8hfkCQTAHBH8PutwFD0krwIka9FldhPZQSUnA+tPD7i0O
kE/hn+2S64k9odjmYTTd1jlS0G80xn533gv7vp/Py7LvSmt00Lfls6/6YaqF+sXh
2pCFqIHPdQnLX7Bx9IZueIICGNlP1xB3v5W8xBTe34wjgBbdXJjQEpIqtfHaYjkZ
hqQFZtMHx0rdHphd9pj9gSoEZhwDcJUnJWhgTrl1xS+gjRDm9Uk22SHXTnCZMamy
JWZiN/0D+c3NUJLinhC8kQdUg8FKen0rSf+VNOKRSppNH+2OhJGwFGbqtCULGfVj
nAJ062HENZ9cuottNZk2/cR5DAcl6Fo9C670U+h1hXpWhFiATroD1PKam4RB0LyD
hs3KYMahwwfgo//BqvbFgDkoSbEuI1jSneeizsMAEQEAAYkCJQQYAQIADwUCU2jt
cgIbDAUJB4YfgAAKCRAO52YWzFgb4UTyD/wMFPYpOMm2L5WoCb0td6X0Zh6hlimA
8wDu7lQ0nKv4cNPBDZ3ysNhyH0VEflVO1ODA7kPZKEpB0QB1oGZwOf9ecSIIpbd1
JpARQ/r8g4GF4IkoMgbenh4pr8IBZjuebpasquy3gPL7oqnABi+za3I/K5wZppj7
7QCP9efXTzUT0tYlgrT6yEeikJRK2EpphsIueqDSrLlABCdceenbnlp+UCTmW3EV
VropLs2pwO3qxFtWV8DxUqe7k59iL6vqGgIIDWgqI+m2eQRgt2SVrzwyucywm0vt
9j84OsG5/dSU0dmoZhO9kNiAWvnTnKIpcQEYKnmKdA4pUPhx2L+I2Fyz+b9JPpKp
2HBVMBWU89nemsqPm+sIrn3hrLv19xU8ZNid/kaBF10ftPRiiUTn2zRXwseUKZPa
Fn2DLd9Woxij7zL+iBKfX90fxvhpoUIeKnAItPCK0ZC8DPP8pKmb/Ex5AHukqY+T
lkTkcz9R1DnxBA1e4JKQkZ0EHDN8cK5dNw0ZnzSoBlCWBmaV5tC2gaCGksJl/ko/
osYGbMFLkh3PJQ7lksX89YahUABfrDERjmlOTNpTTo+HHH2bmjfml7HVSQhR0UHt
BrjUNfsEGhuAo4572KUxkSMJG0M0Flyq7jBnBhvck0CGNA8JXRQpI9wAm6I1hyl6
sl+x9KGL2Rmn/g==
=NkTc
-----END PGP PUBLIC KEY BLOCK-----
