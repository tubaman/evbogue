---
title: Install Micropolis on Arch Linux with Cower
date: 2014-02-06
---

<img src="/images/micropolis.png" class="full profile" />

Not everything on Linux is brilliant, amazing, and the future. Sometimes, it can be just plain fun.

Such is the case with Micropolis, AKA the Unix port of the original version of Sim City.

For those of you who missed the '90s, whether you were in cold storage or are now 12 years old, Sim City/"Micropolis" is one of the best games of all time. Yes, it might not have 3d explosions or PTSD-giving action. But if you get bored, you can call a firey red monster to attack your town. Bonus! it was designed by [Will Wright](https://en.wikipedia.org/wiki/Will_Wright_%28game_designer%29), one of the most thoughtful game designers of all time. 

The only trouble with Micropolis, is it hasn't quite made it into the stable repositories for Arch Linux yet. So, you have to learn how to install community repositories from AUR in order to use it on your [perfect machine](http://build.evbogue.com/). 

Now, you could use [Yaourt](http://archlinux.fr/yaourt-en) to install Micropolis, but at the encouragement of [jpope](http://jpope.org/), I've started using [Cower](https://aur.archlinux.org/packages/cower/) instead. Cower is a bare bones package manager for Arch Linux's AUR community repository. With Cower, you can search Aur, download packages, and then use Arch Linux's build system to make and install them using the dev tool set that you can install on Arch Linux.

First, let's learn how to install Cower.

To install Cower, you'll need to make sure you have your dev tools installed. This is easy, just

	$ pacman -S base-devel

And then, you'll need to download the cower pkgbuild file.

	$ curl -O https://aur.archlinux.org/packages/co/cower/cower.tar.gz

Untar it

	$ tar -xvzf cower.tar.gz

Enter the folder

	$ cd cower

And then make the package and install

**Disclaimer!!!** I can be a good idea to read the pkgbuild files before you install them. AUR packages are unsupported packages, and could sneak funny code onto your system if you aren't careful.

	$ makepkg -si

Then it'll take you through some build steps, and ask you if you want to install cower.

Now that you've installed cower, it's easy to get the pkgbuild for micropolis-git.

First, we'll search for micropolis

	$ cower -s micropolis

And we'll find out that it's called 'micropolis-git'

	$ cower -d micropolis-git

Then move into the folder, and install it

	$ cd micropolis-git
	$ makepkg -si

Type 'yes' to install, and then you'll be able to play Micropolis!

### Build yourself a Sim City, er "Micropolis"!

Because some of you are 12, and missed the golden years of game design, I'll take a brief moment to explain to you the rules of Micropolis.

1. There are three ways to zone your city, residential, commercial, and industrial
2. You can build roads, rail, and power lines
3. You can build coal or nuclear power plants
4. Use police to battle crime, and firefighters to stop fires

The key to Sim City is to keep residential, commercial, and industrial zones in balance as you build your city bigger. The green/blue/yellow indicator on the top left of the screen will tell you what zoning your city needs the most. 

Once you get Micropolis installed, choose a map, select 'easy', and get started. Then make sure you turn off disasters.

Here's what your initial starter city could look like...

<img src="/images/micropolis2.png" class="full profile" />

Now go build worlds on your [perfect machine](http://build.evbogue.com)!
