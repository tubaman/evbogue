---
title: The Exorcism of Steve Jobs
date: 2013-10-15
---

Programmers talk about The Ghost in the Machine, or the Ghost in the Shell. The ghost is whenever something happens in a program that isn't expected and cannot be replicated. The code takes on a life of its own, independent of the programmer, and starts doing things no one ever thought it could. This isn't the exact philosophical definition, but it's close.

One of my primary missions, starting in late 2012, has been exorcising the ghost of Steve Jobs from my machine. Steve's been dead for more than two years now, so it felt to me it was time to move along. I'd mourned the loss, now it was time to get out from under Steve's shadow.

Without a proper exorcism, it can be hard to innovate. When I lived in Seattle in 2011, two ghosts were present almost everywhere. The first was the ghost of Kurt Cobain, who blew his head off in 1994. The second was the ghost of Microsoft. Which is still alive, we just aren't quiet sure why. With Kurt's ghost around every corner, it was hard to get any work done without the ever present feeling that 'all in all is all we are.' 

Steve died while I was living with the ghosts of Seattle. A month after Steve Jobs died, I tossed all of my stuff into a bag, dropped my key into the building manager's mailbox, and walked to the Amtrak station on the south side of downtown Seattle. I felt melancholy, but also relieved, to be leaving the city dominated by so many ghosts.

I felt my spririt lifting as the Amtrak moved down the coast to Oakland. Where I had a cup of coffee, and wandered around San Francisco for a few days. But something felt off in The Bay. It felt empty without Steve. Just as my Mac began to feel empty without Steve. A few days later I booked a flight from SFO to Puerto Vallarta, MX. But Steve's ghost was still with me.

Every time Apple releases a new product or an update to their software, it's as if a million voices cry out, only to be silenced in disappointment. The reason for this, as far as I can tell, is the absense of Steve's ghost. The Mac Pro is a trashcan without the ghost. A iPad Air just seems dumb without the ghost. 

By the end of 2012 I was determined to exorcise the ghost from within my machine. It started when I installed Linux for the first time. I fought Apple's authoritarian firmware for days before Linux finally took for the first time. I started with Ubuntu, but over time I became frustrated with the cracks in Ubuntu's all-in-one approach. Nothing ever worked right. Eventually I heard about Arch Linux, and became obsessed. By this time I was using my even older Macbook Pro. 

I finally got Arch Linux to take on this machine. I could still feel the conflict between the ghost of Steve and the software I was attempting to run on the system he designed.

Then I put my Macbook Pro on ebay. I sold it for more money than I used to go out to a Walmart and buy a cheap Intel laptop. I [built my perfect machine](http://build.evbogue.com).

And now there are different ghosts in my machine.

