---
title: How to Redesign Your Website
date: 2014-04-07
---

Designing your own website is a lost art. 

The best website as of 2014 has zero styles, minimal photos, and no bullshit. People don't want to see creativity in your design as of 2014. 

I'm always in the process of redesigning my own website. I mean, look at it right now. It's being redesigned. A website is never done, a website evolves. In order to keep evolving your website you will need to set aside the time to do the work necessary to keep your website relevant and interesting to the people who read you.

This is not a weekend warrior type of job. No professional painter succeeds by dittling around with paints every other weekend, as no professional web designer succeeds by having occasional passionate flings with HTML.

The first step in redesigning your website is learning how to write basic HTML5 and CSS3. Without these fundamental building blocks, you will be staring at your Vim file not knowing how to do anything. You can't paint a painting without brushes and a canvas, just as you can't create a website without knowing your basic HTML tags.

This article, however, will not cover HTML5 and CSS3 tags at all. To learn these, pick up a copy of [Design Your Website](http://design.evbogue.com). I it to help you learn the fundamentals of web design. These days, it's impossible to find a decent guide online to learning these skills. This is why I wrote Design Your Website, and why you should read it. 

In the meantime, let's talk about what you need to consider when redesigning your website. 

#### Your blerg copyright

Let's start with my ultimate pet peeve, your copyright date from 2012. There's nothing worse for how people perceive your digital presence than to have them visit your website only to see a copyright date from 2012. I realize the last few years have been very hard for digital entrepreneurs, but that's no excuse to forget to update your copyright. For example, I knew Google was dying when I started to see Copyright 2013 showing up all over the place in March 2014.

The simple way to eliminate this problem is to not put a copyright date anywhere. We all know copyright is busted anyway, admit it and don't make your website three years out of date by never updating it. This is no excuse to take the dates off your blog posts though, there's no surefire way to know that a blogger or a blogging platform is a has-been than to visit their undated blog posts.

Anyway, start by doing away with the copyright date. People will steal what they want from you anyway. And there's probably not anything on your website worth stealing, especially if you can't figure out how to update your copyright date.

#### Choosing a website generator

As fun as it is to code an entire website from scratch, it can help to employ a robot to automate some tasks for you. To do this, you will need to choose a static website generator. I use [Hakyll](/howishakyll), but you can use whatever you want. Except for Wordpress, because it's sure sign that you it is amateur hour over at your website. No one who designs professional websites uses Wordpress anymore. Wordpress does not work well with a version control system, so it's very easy for a botnet to attack your website and turn it into a zombie website that attacks other Wordpress websites. Don't be a zombie, use a [static website generator](http://staticsitegenerators.net). You'll be able to sleep at night. You also won't be an asshole who is contributing to the destruction of the Internet by harboring a botnet.

The best static website generators let you throw a whole bunch of .md (markdown) files into a 'posts' directory and will render them up to you using some basic metadata, such as

	---
	title: My blog post
	date: 2014-04-05
	---

	Blerg blerg blerg

You can edit blog posts with Vim and check them into Git. Then, you can use Git to push the blog posts to your server. There's no better workflow. CMSes are so 2009, and Dreamweaver is so 2005. You'll save yourself tons of clicking and other dumbassery by learning how to use a command line static website generator. You also won't appear to be a bedwetter if you know Git.

Most static website generators will let you create post and pages, written in markdown, but also give you the option of coding more complex things if you feel inspired to make a brilliant mess of things. Static website generators will be coded in various programming languages. Choose the language you want to learn the most. Don't pick Ruby unless you want to [live in a ghetto](http://harmful.cat-v.org/software/ruby/rails/is-a-ghetto). 

#### Choosing a design

When you redesign your website, one of the first things on your mind will be how you will choose your web design.

Back in the day people would pay web designers to design websites for them. Now it's clear no one will care more about your website than you. People who are serious about being active and (more important) earning citizens of the Interwebz need to take web design into their own hands.

I want to encourage you to start your web site without any design at all. Just delete the CSS file and get familiar with the structure of your website. A CSS design can always be tacked on after you have a firm structural foundation. Too often people over-design their websites. Over-design is just annoying to most people. When browsing the web, we are making constant decisions about whether or not we want to read things. Too often we land on over-designed websites, and just hit the back button because we don't want to deal with your creativity. 

Web design cannot be too simple. I've seen so much creativity that I want to shoot myself in the head when I land on another creatively designed website. You have to be VERY talented to get ahead in the creative web design game. I don't know anyone who is a decent creative web designer. It's not a path worth persuing. When in doubt, take your web design cues from [Stallman](http://stallman.org), he's lived on the Internet since it was born and knows what is up. 

Purchasing or downloading a "Free Wordpress Template" makes you look like a bedwetter.

#### But I want a pretty website

Alright, you can have one. If you must. But it won't make Stallman happy. Or me. If you're going to go the pretty route, you best know what you're doing. Pretty has been done so many times we (the denizens of the Internet) are sick of it.

Since 2012, it's been fashionable to choose a front-end framework, such as Bootstrap or Foundation. This may be a good option for you, but there are a couple of things to consider. Bootstrap is played out. We've seen so many Bootstrap websites now that we are sick of seeing them. Foundation is a little less popular, but it is also clunkier, which is why it's less popular. I was fond of Skeleton for awhile, but then I looked at my website on a giant screen and it didn't look very good, so I designed my own web framework [Reserva](/reserva). But I'm not using Reserva right now, so what does that say about me? 

If you want a pretty website, you have to make it responsive. HTML is almost 100% responsive out of the box, it's only when you start to get 'creative' that things get whacky. Have you ever seen those fish tanks where they put the tacky backgrounds on them? Uhm, well, they're tacky. You don't want a tacky fishtank-backgrounded website.

#### Start with your structure

I want to encourage you to start designing your website from the ground up. Similar to way you would [build your perfect machine](http://build.evbogue.com), starting from a basic Linux install and then installing programs as you need them. You should start redesigning your website by generating a website without style at all using your static website generator. 

Instead of grabbing someone else's design, and ripping out everything you don't want, you can instead start with nothing and build your own design from scratch. Then you can build the web design you want to build, instead of living with someone else's design. The added bonus of this approach is you will know what you designed, so if you need to change anything you will know how to do what needs to be done.

This is a extreme approach, but it is the only way for you to be comfortable with your website redesign. If your website is the most important aspect of your digital self, shouldn't you know it inside and out? 

To do this, rip out the CSS file. Delete this

	<link rel="stylesheet" href="/static/style.css" />

Don't worry, you can always add a new one.

#### What next?

Now you have a static website generator you can program in your favorite language, and a new CSS file. It's time to start coding up your style. Pick up [Design Your Website](http://design.evbogue.com) if you aren't familiar with your HTML5 and CSS3 tags. Then go to town experimenting with new text styles, padding/spacing, and installing the front-end frameworks of your choice.

Get out there and redesign your website! Or better yet, keep Stallman's dream alive and don't use any design at all. Or you can go back to the bed wetters with that Wordpress theme from 2009. But don't expect any mad respect if you do. 

-Ev



