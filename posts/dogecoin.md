---
title: Why I bought 10,000 Dogecoins
date: 2014-01-24
---

**Disclaimer**: Nothing in this post should be considered 'advice' or 'me advising you' to buy, sell, trade or otherwise lose your valuable money. Cryptocurrencies are an experiment, if you want to get involved the best thing you can do is learn as much as you can so you can make your own informed decisions. And don't be an idiot, store your cryptocurrencies somewhere safe.

***

**Please note I no longer hold Dogecoins as of 04-19-2014**

Last night I bought 10,000 [Dogecoins](http://dogecoin.com/). Here's what happened and why I did it.

It's hard to be involved in tech in 2014 and not have heard of a blockchain. After Bitcoin hit $1000 late last year, the cryptocurrency 'scene' has flooded with speculaters, hype-mongers, and legions of novices.

All of this is to say, the cryptocurrency scene is a rough one. Enter at your own risk.

The good news is, more people know about cryptocurrencies, the bad news is the noise level is so high around Bitcoin and it's ilk that it's hard to figure out what is working, and what isn't in the space.

One of the recent trends in cryptocurrencies has been the explosion of altcoins. Altcoins is the generic term for cryptocurrencies that are not Bitcoin. Many of these altcoins are worthless. Some are called 'scumcoins', which means they are to be avoided. 

But a few have stood out. Litecoin, Primecoin, Devcoin, Namecoin, Worldcoin. Each of these coins has significant distinguishing features from Bitcoin itself. 

And then there's Dogecoin.

Dogecoin, as far as I can tell, started as a joke during the altcoin explosion. Someone threw together a fork of Litecoin with many more coins available and lower difficulty, and Dogecoin was born. The difference between Dogecoin and Bitcoin is there are many many Dogecoins, and they're all virtually useless.

In Mexico there are little .10 peso coins. They're tiny, and appear to be made out of a piece of plastic covered in tin. These coins are worthless, so people just throw them on the ground. Most people try not to use them at all in transactions.

Dogecoins are in the same league as these shitty .10 pesos coins in Mexico, they're worthless on their own. One Dogecoin is worth 0.0019 USD at the time of this writing.

But now that Bitcoins are worth so much each, every transaction ends up being a decimal. I bought 10,000 Dogecoins for .02 BTC. 

This vast difference in values puts Dogecoin in a very different brainbucket from Bitcoin. Where people are likely to horde Bitcoins (and they do), what's wrong with throwing Dogecoins around? This has led to Dogecoin's transaction volume going through the roof, in comparison to Bitcoin.

Oh, and Dogecoin has a cute mascot.

This is why I bought Dogecoins. Not because I want the most valuable cryptocurrency. On the contrary, I want a lot of cryptocurrency that I can throw around without worrying too much about losing it. This brings the fun back into cryptocurrencies, and I'm not so worried all of the time. 

Here's a [Dogecoin chart](http://doge.yottabyte.nu/), so you can see how much my 10,000 Dogecoins are worth in a week or so.

