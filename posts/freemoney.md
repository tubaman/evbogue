---
title: Free money
date: 2014-07-05
---

There is this terrible idea I want to tell you about. If you visit the Internet, you have probably heard of it, because almost everyone is talking about it. 

It's probably one of the most popular ideas on the planet.

I will be the most unpopular person on the planet, because I'm about to call bullshit on it.

This idea is basic income. Everyone in the United States should get $20,000 USD or maybe even $40,000 USD per year, for free. Then they can spend it on whatever they want. You want to buy heroin? Use your basic income. You want to buy [Develop Your Business](http://dev.evbogue.com)? Use your basic income. You want to buy food? Use basic income.

There are two huge problems with this idea.

The first is the people giving away the free money will no doubt be people in power. The central government. And we've all seen how fracked up their priorities are over the past year 

The next problem is basic income will probably just lead to $40,000 being the new zero.

The first problem is the very bad one. What is to stop my basic income from being cut off because John Kerry doesn't like what I think of him? All it takes is for him to place one decent phonecall, and I'm a slave and he's my master.

I realize hating on basic income is the least popular thing on the planet for me to do. Everyone who supports basic income doesn't have any money, that's why they want free money. This means I have the potential to anger a hoard of very hungry Americans without any other options than to dream about basic income.

I'm not denying that there is a huge problem with the economy right now. But central planning isn't the way to go. Have we all forgotten Stalin and Mao? Well, I haven't. A lot of people starved to death. They starved because someone had a great idea, very similar to basic income, that was very popular with the poor people of their countries. But starving is never popular, as Mao and Stalin both found out.

I have this huge worry that basic income will make the USD hyperinflate so fast that your hamburger at McD's will go from being $7 to $20,000 overnight. Then what are you going to do with all of that basic income? 

I can see basic income working somewhere small, somewhere with a lot of rich people. But look at Singapore. They don't have basic income! No one is dumb enough to give Singaporeans free money. Instead they give them an un-free press, the death penalty for doing or selling drugs, and $5,000 fines for things you may or may not have done. If you're a natural born Singaporean, you best not rock the boat, because your housing will get taken away from you. If you're a Singaporean you'd be dumb to blog, or write anything at all. In fact, having been to Singapore just two years ago, I can tell you with 100% certainty that Singaporeans never ask any questions at all. 

Why? Because they are afraid someone will take away their housing if the government finds out that they've been talking about anything at all. So Singaporeans just hold very still and hope no one sees them in a city full of cameras, active military, and Singaporean spies trying to rat out their fellow "captalists" who live in government housing. As we call them in NYC, The Projects.

Would Singapore be different if instead of paying for everyone's housing they instead gave them $50,000 SGD? No. Because eventually someone would be telling them what they can and cannot spend that money on. Money from the government is NEVER no-strings-attached money.

So you think free money from a central government is going to bail you out of broke-ass-serfdom. Instead it spells inevitable death to any creativity you had left in you.

Then there's this other big huge question. If the United States is nearly 18 trillion dollars in debt, where will they get all of the money to pay that debt off? Definitely not from people who are lounging around on basic income. Nope. They're going to get it from steal from the rich and giving to the pockets of beaurocrats. 

Just imagine the beaurocratic nightmare that would be basic income. How much do you have to prove who you are to get it? Do you have to give regular checkups with a case-worker to make sure you're not spending that money on heroin? What if you murder your friend, and take his basic income too?

The other thing is, how much more muggable will people be when they all have a guaranteed $3333 USD on them every single month?

How much more will rent be when everyone has a guaranteed $3333 USD on them every single month? I'll tell you, all apartments will now be $3333, and you will be left wondering what happened to your foodstamps. That's what will happen.

You know what. If basic income happens in Los Estados Unidos, this is what I'm going to do. I'm going to take that basic income, and use it to get as far away from there as possible. 

Because basic income, no matter how popular or good intentioned, will bring about the end of times in America. Faster than anything you've imagined. 

You think not? Argue with me. If you have the beer calories in your bloodstream to do so. While your beer still only costs $2. Because once basic income hits? Your beers won't be so cheap anymore, will they?

