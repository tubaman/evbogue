---
title: Just get a job
date: 2014-06-15
---

I can't tell you how many times I've heard this over the past few years

"Just get a job!"

"If I only get a job..."

"But if you only had a job..."

The trouble is that it isn't acknowledging what almost everyone knows. There aren't any jobs to get.

Oh sure, you see 'we're hiring!' signs all over the place. But the reason these signs are out aren't so you can 'just get a job'. They're out so you don't burn the place down after they tell you no. 

Most storefront companies have instructed all of their employees to say "sure, we're hiring. Apply online, and we'll be in touch."

The reason companies do this is so the chronic unemployed of America don't damage their stores or hurt their employees for being honest with you.

So part of the problem is no one is being honest. There are no jobs.

Yes, the unemployment rate has "come down!" to only 7.4% in los Estados Unidos. What they don't tell you is 7.4% is how many people are **getting unemployment checks**. The rest are no longer looking for jobs. This means they are living in basements or cardboard boxes.

"Just get a job!" Is the most absurd advice you could ever give anyone.

We can only hope it won't be absurd advice after John Fucking Kerry and Obama get WWIII kicked off. Then, maybe, there will be plenty of jobs. Plenty of jobs making weapons to kill people. And also plenty of jobs to step into after the people who were in them before get nuked.

But right now, there are very few jobs. Oh sure, there are exceptions. As you wander around your local American city you will see people who do have jobs. The three people who work at the gas station. The 250 people who work at Walmart. The 54 people who work at McDonald's. The 35,000 fuckwads who work at the NSA. 

But you have to understand that the line for these jobs is so long you will never reach the end. That's why they don't let anyone line up for jobs, because if people did other people would see just how hard it is to "just get a job."

The other branch of jobs is what I love to call "socialist jobs". These are the kind of jobs everyone has in North Korea. These are government jobs. These are the jobs that come from 18 trillion dollars in debt. Of course there are socialist jobs. These are the jobs that promise you a 'big payday!' at the end, but then only deliver to people who were born when Roosevelt was president. 

Another job is the kind you see being advertised almost everyone in America. These are jobs you have to pay to get the opportunity to have. The jobs that come, but only after you "get certified." Where the certification course takes three years to complete and drains your entire bank account that you didn't have any money in to begin with. Socialist jobs may be evil, but certification jobs are scams. It's a clear sign the entire economy is broken when you have to pay some sleezy motherfucker for three years to perhaps have an opportunity to 'just get a job.'

People who tell you to 'just get a job' aren't testing. They aren't experimenting. First of all, they aren't looking at their own lives, because if it was 'so easy' to 'just get a job' then why don't they have a job? Hmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm?

And if they do have a job, do they want to ever consider quitting that job? No because if they quit they wouldn't be able to find another job. They think they are an honored priviliged person for being able to 'hold down a job.' meanwhile they are working 80 hours a week for $24,000 a year. In the other three hours they tell you to 'just get a job.'

I think we need to give up telling people to 'just get a job'. We need to realize that the jobs aren't coming back, and the American socialist suburban dream has died with all of the jobs.

And now it's time to improvise. This means it's time for entrepreneurs. It's time for hustlers. It's time for waking up every day, running out the door, and finding other ways to make money besides 'just getting a job.' 

In la Ciudad de Mexico no one says 'just get a job.' Instead they say 'go make some money.'

But it's not illegal to sell whatever the hell you want to whoever the hell you want here.

In los Estados Unidos if you try sell anything to anyone you're liable to end up with the other 2.3 million people who are propping up the great prison jobs you can 'just get' up there.

So what is there to do? What are we going to do?

First of all, let's stop telling people to 'just get a job.' Everyone knows it's absurd anyway. You're a dumbass if you tell anyone to do this. 

Next we need to let ourselves off the hook on 'just getting a job.' throw the resume away. Resumes don't get people jobs, economies that work do. And the economy en los Estados Unidos is an economy of homeless people and people on foodstamps. Yes, there are a handful of jobs. But can you 'just get a job'? No.

Maybe hyperinflation will be the magic cure. Or maybe Romans had to find new ways to hustle after the fall of Rome.
