---
title: If you don't know Git, you don't know shit
date: 2013-12-12
---

This is an interview with [Gwen Bell](http://gwenbell.com) about her book project, [Git Commit](http://git.gwenbell.com/). The interview was conducted using [IRC](/irc) on the date above. 

Below we talked about why she rewrote 70% of the book, what she's learned about Git, and how shit can roll up hill. Also, we ask the question on everyone's mind... **has Git destroyed The Valley?**

**Ev:** You just told me you re-wrote 70% of [Git Commit](http://git.gwenbell.com). Why?

**Gwen:** I started writing Git Commit in my grandparent's basement in 2012. One year ago this week. I've been on the command line every day since then. Let's just say, it was time to edit Git Commit. So I did!

**Ev:** How has your understanding of Git improved over the past year?

**Gwen:** Wow. It has changed in every way. This month, for the first time, I'm managing a distributed team with Git. People live all over the world, in all sorts of time zones, and they do their commits at all hours, and then push up to their branches. And then I do a merge. That is an amazing way to work with a distributed team.

In fact, it's the only way I'll work with a team in the future, until something better than Git comes along. And I'm not holding my breath for that day.

So, there's that. Then there's learning to work with remotes. Now that I'm on a VPS on the fastest (far as I know) tunnel in the world, when I clone something down it's hella fast, doesn't matter where I am in the world. That's part of the beauty of Git. You can be on a beach in Nicaragua. Do they have beaches? And using your remote VPS, and it'll be as though you're sitting on 26th in NYC or wherever.

That, and after this year, I mean, I knew it was coming, and so I started learning Git and cryptography in mid-2012, but after the Snowden revelations it has become evident we need to take care of our digital selves, better care than just publishing to wherever and hoping nobody accesses it.

So now, I know SSH inside and out and if there's a man in the middle attack on my server, I will know it.

That allows me to sleep at night. Better than I already did.

**Ev:** I know you get this one a lot. Why should I learn Git?

**Gwen:** Sometimes I liken what happened this year to a security breach when I was a child. I was ten. I kept a diary. 

My babysitter, let's call her Loudermouth, found my diary and sat at the top of the stairs reading aloud from it. 

From that day forward, I started writing in code, I started deleting words and phrases, and the only one who would know what I'd deleted was me. That kind of code gave me a buffer; I continued to write, and in fact, became a professional writer, but I had to have that buffer.

GIT is your buffer.

Git gives you protection, reach, longevity and privacy. 

Those four things are musts. They aren't optional; they're needs. They're the bare minimum. Now, I don't have an air gap the way Laura Poitras does; I'm not working on anything with high level security clearance. But believe me, if I were, I'd be using Git at a bare minimum.

**Ev:** The question everyone's been waiting for. Why do you hate on Github so much? Why Git not Github?

**Gwen:** Ok, so they have done an amazing job of conflating Git and GitHub. Right? That's marketing.

GitLab is amazing. It has come so far in one year.

If you're only pushing up to one remote repo, you are missing the glory of Git; DISTRIBUTED version control

That and GitHub rubbed me the wrong way earlier this year when I pitched them on an idea. I pushed back with them and my message was cc'd to oh, half a dozen people in GitHub. I was told it was a "flat company structure", which pissed me off. And I started looking for alternatives. I found Gitboria (an added level of safety; CJDNS) and GitLab, which has private repos for FREE. So, now I use those two and ignore GitHub.

**Ev:** And Gitlab LOOKs better than Github now too, right?

**Gwen:** Right.

Bless, I mean I feel for GH at this point. They've alienated people, including me, and there's no reason to stick around now that GitLab is out there, free and open source.

**Ev:** Do you really feel for Github?

**Gwen:** Yeah, I do. There are spamurai there who have kids to feed. 

And I have seen a company post notes to the outside of the building, "we're closed; no more paychecks for you". I lived in Japan, Quantus flew some of their peeps home because people were so unprepared for the suddenness and the unthinkingness of the layoffs.

No I don't feel for GH as an entity, I feel for the humans who will be royally fracked when shit eventually rolls downhill; which, it will.

**Ev:** How can shit roll down hill in a flat company?

**Gwen:** Someone will pick it up and turn it 45 degrees, is how.

By turn, I mean tilt, like those arcade games you play with pinballs.  With the naked ladies on them.

**Ev:** Will Git destroy The Valley?

**Gwen:** GIT ALREADY DESTROYED THE VALLEY

**Ev:** Why?

**Gwen:** Oh man, you love this question so much, you answer it.

**Ev:** But this is your interview. But alright. We'll break protocol, but just this once.

**Gwen:** Good because I am getting the hunger.

**Ev:** I love love love the [Linus Torvalds talk at Google in 2007 about Git](https://www.youtube.com/watch?v=4XpnKHJAok8)

**Gwen:** Great talk.

**Ev:** It should be subtitled, and probably will be "how Git destroyed Google"

Don't watch this on Youtube people! [youtube-dl](https://www.archlinux.org/packages/?q=youtube-dl) it! Then you can watch it in high res, without ads, and without stopping every five seconds to buffer.

**Gwen:** (you'll need to be on [arch.evbogue.com](http://arch.evbogue.com) to do so)

**Ev:** You can see the horror on the faces of these kids. How will we sit at desks if there's a decent distributed version control system?

Git took the politics out of open source. Git made it so all of the petty discussions could stop, and people could get down to work. They could do branches, and merges, and code. Whenever someone wants to argue with me, I just say 'do a merge request' and they shut up.

**Gwen:** Whoopsie! Can't eliminate the drama, eh?

Right, I wonder if now that there's been a security breach re: Linus's code base on GH, we'll see him move elsewhere.

**Ev:** Since 2007, there's been an explosion of open source alternatives to things. Linux is finally a useable operating system because of Git.

Yes, Linus should have the balls to leave Github after that. No sense in staying there, repping for them, if he's not getting a paycheck.

**Gwen:** Right. I would love to see him also leave Poogle Glus but he is his own man.

**Ev:** Alright, let's wrap this thing up. Linus can figure out that Google+ is shit on his own.

**Gwen:** Why should people [buy Git Commit](http://git.gwenbell.com)?

1. Learn Git and be miserable like me!
2. Learn Git and pull yourself into the probably top 5% of all developers out here.
3. Learn Git and get access to all public repos and any private ones you're given access to, and know how to behave with said repo; you're not in open source if you don't know how to manage repos
4. If you're a writer, learn Git and pull ahead of all the writers using Poogle Docs, Scrivener and all the other non-distributed non-version controlled apps
5. Learn Git and have an ongoing affair with something you're going to be learning for a long, long time. I'm still learning, and I'm a year and a half into this.
6. Learn Git and give yourself the freedom, peace of mind and longevity that comes with distributed version control. Who CARES if someone steals your laptop? It's disposable; your work isn't. With Git, it won't matter, you'll just clone it off your VPS and badabam you're back to work.
7. Learn Git and if you already have, Give the Gi(f)t to someone else and I'll send them a hand-written note, if you buy between now and December 31.

In fact, I'll GIT COMMIT them a note.

By hand-written I mean digital hand-written.

**Ev:** Better plan, given the postal service in this country.

**Gwen:** Don't go getting all cray on me.

Righhhhht.

Thanks for reading, y'all.

**Ev:** Alright, I'll edit this up after tacos and git commit it.

**Gwen:** And may your repos be plentiful.

***

You can buy [Git Commit](http://gwenbell.fetchapp.com/sell/yiniekoh/ppc) using this link, or visit [git.gwenbell.com](http://git.gwenbell.com) for more info.
