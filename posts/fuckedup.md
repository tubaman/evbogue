---
title: Admitting when you fucked up
date: 2014-06-11
---

Fuck. I messed up.

I'm standing in line at the Interjet terminal. I walk up to the lady

"en espanol o ingles?" she says. "como es tu espanol?" 

"mas o menos" I said

"Colombia requires you to have an onward ticket. Do you have one?" she said

"I was planning on booking a bus ticket to Ecuador." I said

"Do you have the ticket on you?"

"No, I haven't booked it."

Inside my brain *fuck, Ev, you fucked up.*

*Duh, you're trying to leave on a one-way ticket from the illegal immigration capitol of the world.*

*But you've done this before! You've flown one way to Singapore, to Japan, to Berlin, to Mexico a handful of times.*

"There must be some way I can get on this plane..."

"No. You need to book a ticket and come back. You have until 8:47."

I walk away. It's 8:15.

I try to find Internet Wifi. It barely works. My battery is almost dying. There are no plugs anywhere. I try to find a bus ticket I can book, but I can't find one anywhere. Then I start panicking. I look at my Paypal balance, and I realize I barely have enough money in my account to book a ticket and also eat and also afford housing. Fuck.

I wander around the airport in a daze. The checkin deadline comes and goes. I get very mad at myself **you just blew $279 on a ticket you didn't even use!**

I try to find an ATM that works, so I can get a coffee, but none of them seem to be operating. One of them grabs my Paypal card, nothing happens, and it doesn't spit it back out again for three minutes. I have another panic attack.

**But who the hell did you think you were anyway? Buying a one-way ticket into a country you've never been to. Going towards JULY of all fucking months? July is when everyone leaves the Internet and you have to work your ass off to even make rent? And you're trying to TRAVEL? Yo, fuck that motherfucker who said you should be aspirational. You're not aspirational. You should stay out of the travel business and stick to the tech business. No one has money to travel right now anyway. Shit is fucked.**

I turn around and head towards the Metro. At Pantitlan there are 100,000 Mexicans all trying to get on the same train at the same time. I get sardined into the Metro, and start heading back into the City.

All I can think is...I must be a Mexican now.
