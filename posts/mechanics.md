---
title: The Mechanics of Publishing
published: 2013-12-04
---

+ Do you want to publish to the Internet? 
+ Do you want to people to read what you have to say?
+ Do you want to get paid to write? 

Then we have to talk about the mechanics of publishing first.

You can't talk about earning in the digital space until you first talk about the technical skills you'll need to succeed in the digital space. In fact, probably the only 'safe thing' to talk about is the mechanics of digital publishing.

Measuring success stories, from case to case, is difficult. Too often I have conversations with clients who believe in someone-or-other's specific strategies for 'publishing success' only to not be able to repeat this success themselves. The reason for this failure is easy to spot: blind following and lack of experimentation. It's best not to listen to any specific strategies people are advertising in the digital space, instead opt for cold/hard/rational facts. Instead of listening to bullshit, split the truth from the bullshit with your own measurable experiments. 

When you experiment, and only then, can you know the reality of your own situation.

#### Publishing to the web

As much as this company or that might want to convince us, the best place to publish is still the Internet using a distributed platform known as the world wide web. When you publish using HTML and deliver your content over http, you're using a distributed system. Anyone can fire up a website and publish something to the Internet. The middlemen you have to deal with in this case are Icann (for domain names), and your web or VPS host (for a web server.) For a web server, I recommend [Digital Ocean](https://www.digitalocean.com/?refcode=26d8ed49730d) (rewards link)

Unlike social networks and app stores, it's very challenging to censor the distributed web. Most Internet connected devices are able to browse the web, so you get the biggest audience as possible for the work you're doing. The web is also an open platform, so you can customize the experience for your readers as much as HTML, CSS, and JavaScript let you. I talk more about this in [Design Your Website](http://design.evbogue.com/).

Now, some people will tell you publishing to the open web is a bad idea. They'll say you'll get more 'play' if you use a closed system where some higher power decides who gets access to your work. Relying on a centralized system for distribution can, in some cases, have advantages. But most of the time it's just a great way to give most of your profits to a company bigger than you. To borrow a phrase from Richard Stallman, "[don't do business with Amazon](http://stallman.org/amazon.html)."

While it might be tempting to give away 70% of your publishing profits to Jeff Bezos's yacht fund, it isn't worth it. No matter what the bullshitters tell you. 

Publish to the world wide web, keep your profits, and make your work accessible to all.

#### What formats to use?

The bad news is, no matter how much we wanted .epub to be a format people embraced, this has not been the case. Ebook readers are relics from 2010-2011. People read on computers, tablets, and cell phones -- all of the above are connected to the Internet and can use web browsers to access digital downloads. Why? Because the difference between epub and html is marginal. None of the epub stuff got implemented, because HTML was already implemented and why re-implement a decent thing? 

PDFing your book is no longer 'cool'. PDFs are intended for printers, and very few people print ebooks off anymore. We stare at computer screens all day, why not continue to stare at them while we're reading your digital book? Reading a PDF on a computer is almost always a terrible experience. The reason for this is PDFs are not responsive to screen sizes. As screen sizes have proliferated, responsive web design has taken off to cater to them. PDFs, on the other hand, are still intended to give a great experience when you're taking your book to the printer. And who wants printed books anymore? Only the nostalgic. Not me.

#### Maintaining your content

Content. Content. What a horrible word for the beautiful artful words you're committing to your keyboard. I hope you'll never begin to think of your words as content, but for this section the word applies. How do you keep track of your content as it moves through space/time? 

I now use [Git](http://git-scm.org), the brilliant version control system built by Linus Torvalds, to track my content as it moves through space/time. Why? Because it gives me accurate information as I move my words through space/time. I say space/time here because I mean it. Moving your words, especially as they mature, is a giant project if you aren't using a system built for maintaining lots of characters. As one example, try cloning down my website and checking out a commit from six months ago. 

	$ git clone https://gitlab.com/ev/evbogue.git
	$ cd evbogue
	$ git checkout 3033b5a1a4493fa52df8cf086eb3acc803f91f97
	$ npm install
	$ node app

For this to work, you'll need to install Node. For instructions on how to do this, read the free section of [Deploy Node](http://deploy.evbogue.com).

Then navigate to localhost:8080, and you'll see my website as it was six months (or so) ago. Different font, different content, different photo. Cool right? You can see what I was working on 6 months ago without problem. Can you do this with any other software besides a decent Linus-approved version control system? No. So the advantage of using Git is obvious. You can move your content through space/time. But yes, you'll need to learn Git. 

You might find yourself thinking in more dimensions than the average human. This is to be expected.

#### So what now?

It is with great sadness that I tell you that there is no magic bullet gold silk road to publishing success. I can only encourage you to learn a few more tech skills, in addition to continuing to write to the public web. This way, when some brand new magic technology comes along, you can use it to amplify your voice and reach new people. Until then, it's time to lock yourself in a basement, or a hotel in central America, and keep learning hard shit. Because it's the learning hard shit that will set you apart from the riffraff of the Internet publishing on Wordpress blogs and crossing their fingers they will sell one more $1 ebook today on Shamazon -- without looking at the facts of the matter -- that they're broke, tired, and alone.

Ganbatte!
