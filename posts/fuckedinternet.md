---
title: The Internet is Fucked
date: 2014-03-18
---

"Hello, computer."

I'm drinking a Sol. This is a beer. With a feint aftertaste of sweat. But not blood.

Do I often drink and write? Yes.

Do I often write about things that are true and I care about? Yes.

Am I older than 27? Yes.

Then I will probably make it at least into my mid-forties before unfortunate things happen to my liver. Unless the NSA hires an assassin on the Tor hidden wiki to have me offed, or I am killed my a jealous reader. I'm writing this so you know, whatever happens, it was the NSA, and it was murder.

Minimalism is so boring today. Let's talk about futurism.

What I don't understand about you is this: why do you continue to use Gmail? I'm two weeks into my Gmail embargo and I still have this long list of people who can't seem to wrap their heads around the idea that they can send emails using pop3 or imap. Or even better, get on Bitmessage. Get a few of your friends on Bitmessage. The average person probably corresponds with 2-5 people, so why not just admit this and get those 2-5 people on Bitmessage? All of the important people in my life are already on Bitmessage, so if you're not on Bitmessage that must mean you're not important to me. Right?

The real brainfuck of the whole thing is that you read that sentence and realized that the reason you're not important to me is the same reason why you're not on Bitmessage. You can't figure it out. If a person is the sum of the 15 people he hangs out with, why would I want to hang out with you? I'd be one-fifteenth less smart.

Anyway, the Internet is fucked. I want to tell you why.

I'm not going to blame this one on the NSA, though they have certainly taken advantage of the Internet. That's probably all we should say about the NSA, they're the types of people who take advantage, without consent, young American girls who don't know any better than to stream their naked likeness over the Internet. If anyone is watching those vids all day, it's the NSA. Cue hired hitman.

No the Internet is fucked because we aren't technologically savvy enough to encrypt everything we do. We happened onto this magical technology called the Internet, and we played with it for awhile until we happened upon centralized social networks, then we were stupid enough to trust their creators, and now we're left with the fucked up reprocussions of being a race of half-pig/half-chimp numbskulls who don't know how to program distributed cryptographically secure software.

The good news is that it's not your fault. You're just a half-pig/half-chimp who figured out how to walk on two legs.

The bad news is we're not safe using the Internet until we can figure out how to keep the peeping toms at the NSA out of our collective junk.

I'm not here to talk about solutions. I'm here to reiterate the fact that the Internet is fucked. We don't trust it at all anymore. This is bad, because the Internet is one of the most amazing inventions of the past century. And here we staring at empty inboxes (and some of you unfortunate souls, empty Gmail boxes), knowing that if we write anything private or important it will no doubt be recorded for the Obama dictatorship's secret courts to read later and make up whatever story they want about us.

This is just too unfortunate. How unfortunate is this? Too unfortunate. It makes me want to drink myself to sleep every night. So I do.

But I have unlimited beer money, because my overhead is so low, because of minimalism, that I have the privilege of doing so. So while you're shivering in a North American Hole, I'm getting a nice Mexican tan and drinking a Sol while I write to you about futurism.

So who's bad idea was it to put Borg-glasses on our faces, record every moment, and broadcast it to a centralized social network? Sergey Brin's, of course. And it might have temporarily got him laid, which is an amiable position to be in if you're in your 40s. Young hot asian AND jewish? Not many Glass wearing men can claim this fame. But did they record the first Glass porno? If they did, no doubt the creepers at the NSA have a copy they will release to us eventually. Or we'll see it at the Sergey Brin divorce hearing.

If minimalism is your one true god, then what is futurism? Too often it's our one true delusion.

I was wrong enough to buy into The Singularity. But I was right enough to write about fucktards wearing privacy-invading Goggles in Augmented Humanity. But will I be right about much else? I am not a techno-utopian. I'll just follow Gene Roddenberry's view that humanity will descend into World War III before the Vulcans come and dig us out of this mess. Because anyone who listens to the current nonsense coming out of the propaganda department of the Whitehouse straight to a Reddit near you knows that there's some wool being pulled over our eyes. And maybe this a good thing, because us Americans can't look at the horrible reality we live in. An undeveloping country. 

When have we seen an undeveloping country before? Rome. It fell. Remember? Hence our fascination with it. Rome was probably the best place to get 12 naked ladies to give you a pretty sweet massage. But now we just remember it as a near bankrupt state of the United States of Europe. 

So the first Romans had a great time, but later down the line some very smart Romans invented Stoicism. 

Stoicism was an early precursor to minimalism. Because Stoics were also futurists, and they could see that unless they move very very far away that the near future was going to be very very fucked. So they grew out long beards, and you know the rest of the story because you've read your Seneca haven't you?

And the stoics taught us that The Minimalists were not true minimalists, but poseurs who drive around in a car all around the country to try to prop up their rather poor book sales meanwhile blowing through more gas money than they make from the 25% they get from their $4 Amazon book sales.

Meanwhile most of us (smart people) don't do business with Amazon. Because Richard Stallman is a god, and perhaps the most legit person on the planet and we listen to everything Richard Stallman tells us. Except for maybe using pure GNU/Linux, because Parabola GNU/Linux doesn't have the same ring to it as Arch. But I'd totally switch to it if they could think of a decent name. Naming software is everything.

This is why we love Minimalism, because it has the best name on the planet. Mini-mal-ism. It has an ism, so we can endlessly discuss it forever. It has a mal, which is the name of the lead character in Serenity, and it has a Mini, which is smaller than most of the fatties who live north of the border. 

So we aspire to live a minimalist lifestyle. But not the kind of lifestyle that involves driving around in cars everywhere to go to meetups with one or two people, if we're lucky to have one or two people show up to our meetups.

Instead, a true minimalist is speaking so much truth that they don't go to meetups, because if they did they'd probably end up kidnapped and checked into an insane asylum. Like Dr. Bronner.

And the reason a minimalist can tell the truth is because a minimalist doesn't have a mortgage, and two cars, and five kids. Because having those things would mean that we never get any work done. A minimalist doesn't drive all over the country, burning through tons of oil in order to make a few $4 ebook sales. 

A minimalist uses their privileged position of power to write things that people don't want to hear. 

This is why I hope Kanye is a minimalist, because if he is, then he won't have to worry about only selling fifteen copies of Yeezus. Just because no one wants to hear it doesn't mean you shouldn't also say it. For example, [Build Your Perfect Machine](http://build.evbogue.com/) was probably a harder sell than even Augmented Humanity, but that didn't stop me from trying to push it on you every single day. Why? Because learning how to put Arch Linux on your computer is a decent use of the brief moments you have left on this planet.

And what better way is there to improve your life than to improve that shitty computer you sit in front of all day? 

But we were here to talk about the Internet. To be more specific, how fucked it is.

If you want to learn more about how fucked the Internet is, why not read Gwen Bell's latest? [DWIII](http://gwenbell.com) will give you an idea or two about how fucked the Internet is. 

Don't read it just because I told you to. Read it because you know reading something hard to hear is good for you. 

