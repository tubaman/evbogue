---
title: Wash your own fucking clothes
date: 2014-05-18
---

I've been trying to figure out what to write today.

And then it occurred to me. I should write about minimalism! Why not. I'm the last minimalist alive.

But not just minimalism. No. I want to write about washing your own fucking clothes.

When was the last time you washed your clothes?

Well, for me, it was just a few days ago. I live in Mexico, where there is no in-house laundry. If I want to get my laundry done, I have to go down to the launderia. 

The trouble with the launderia is they don't want to wash anything less than 1 kilo of clothes. And I happen to have less than 1 kilo of clothes. So if I want to do my load there, I have to bring all of my clothes, and my towels, and my sheets, and then I manage to hit the correct weight to pay $70 pesos and get my clothes done.

The trouble is, then I have no clothes to wear.

Instead, I've discover an alternative strategy. I wash my clothes myself. 

I realize this is crazy, and I will get endless hatemail about it. However, I've found it is easier to wash my own clothes.

Recipe for washing your own clothes.

1. Get a small plastic tub
2. Take it in the shower
3. Soak your clothes in Dr. Bronners Magic Soap (or other soap) for a 15 minutes or more
4. Take a shower
5. Massage clothes
6. Rinse clothes
7. Hang clothes to dry

I realize if you have shit-tons of clothes, this will be a miserable experience for you. This is why I'm a minimalist, because then I only have to wash a few clothes items at a time.  

The end.
