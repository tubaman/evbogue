---
title: Aspiration 
date: 2014-06-07
---

You were right. I've been trying to listen. You say I'm miserable. I'm drinking myself to sleep almost every night. Something needs to change, and it's me.

I keep saying I want to get farther away from America. I'm going to do this. I'm not going to say where I'm going, but I will tell you when I get there.

In the meantime, I'm working hard on [Undesign the Web](http://undesign.evbogue.com) to get up the funds that I need to travel again. 

Don't worry, I'm going to keep drinking. The idea is to get to a place where I don't need to drink myself to sleep anymore.



